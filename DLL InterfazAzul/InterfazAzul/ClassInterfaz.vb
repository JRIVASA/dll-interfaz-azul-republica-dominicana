﻿Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports System.Reflection
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms

Public Class JsonResponse

    Public DateTime As String
    Public ResponseCode As String
    Public ResponseMessage As String
    Public IsoCode As String
    Public ErrorDescription As String

    Public Receipt As String

    Public ReceiptMerchant As String
    Public ReceiptClient As String
    Public SignatureData As String
    Public RequireSignature As String
    Public QuickPayment As String
    Public OrderNumber As String

    Public ResponseFields() As Responsefield

    Public SaleResponse As SaleResponse

End Class

Public Class Responsefield
    Public Name As String
    Public Value As String
End Class

Public Class JsonResponseCierre
    Public DateTime As String
    Public ResponseCode As String
    Public ResponseMessage As String
    Public IsoCode As String
    Public ErrorDescription As String
    Public Receipt As String
    Public ResponseFields() As Responsefield
End Class

Public Class JsonResponseUltTrans
    Public DateTime As String
    Public ResponseCode As String
    Public ResponseMessage As String
    Public IsoCode As String
    Public ErrorDescription As String
    Public SaleResponse As SaleResponse
End Class

Public Class SaleResponse
    Public DateTime As String
    Public ResponseCode As String
    Public ResponseMessage As String
    Public IsoCode As String
    Public ErrorDescription As String
    Public ReceiptMerchant As String
    Public ReceiptClient As String
    Public SignatureData As String
    Public RequireSignature As String
    Public QuickPayment As String
    Public OrderNumber As String
    Public ResponseFields() As Responsefield
End Class

Public Class RequestState

    Private BufferSize As Integer = 1024

    Public BufferRead() As Byte

    Public RequestObj As HttpWebRequest
    Public SendData As String
    Public ReqStream As Stream

    Public ResponseObj As HttpWebResponse
    Public RespStream As Stream
    Public ResponseData As StringBuilder

    Public CurrentlyProcessing As Boolean
    Public Failed As Boolean
    Public TimedOut As Boolean
    Public RequestWriteDone As Boolean
    Public GetResponseDone As Boolean
    Public ReadResponseDone As Boolean

    Public LastException As Exception
    Public LastWebException As WebException

    Public Sub New()

        BufferRead = New Byte(BufferSize) {}
        ResponseData = New StringBuilder(String.Empty)

        RequestObj = Nothing
        ReqStream = Nothing

        ResponseObj = Nothing
        RespStream = Nothing

        CurrentlyProcessing = False
        Failed = False
        TimedOut = False

        RequestWriteDone = False
        GetResponseDone = False
        ReadResponseDone = False
        LastException = Nothing
        LastWebException = Nothing

    End Sub

End Class

Public Class ClassInterfaz

    Private AllStepsDone As ManualResetEvent
    Private BufferSize As Integer = 1024

    ' Abort the request if the timer fires.
    Private Sub TimeoutCallback(State As Object, TimedOut As Boolean)

        If TimedOut Then

            Dim ReqData As RequestState = State

            If Not (ReqData Is Nothing) Then
                If Not (ReqData.RequestObj Is Nothing) Then
                    If ReqData.CurrentlyProcessing And Not ReqData.TimedOut Then
                        ReqData.RequestObj.Abort()
                        ReqData.TimedOut = True
                    End If
                End If
            End If

        End If

    End Sub


    Public Host As String
    Public HostAlternativo As String
    Public Usuario As String
    Public Clave As String
    Public MerchantID As String
    Public TerminalID As String
    Public Reintento As Boolean
    Public CodCaja As String

    Public Input_InvoiceNumber As String

    Public Link As String

    Public Fecha As String
    Public Codigo As String
    Public Mensaje As String
    Public Referencia As String
    Public Autorizacion As String
    Public RawSignatureText As String
    Public TarjetaNumero As String
    Public TarjetaBIN As String
    Public RawDescuentoAplicado As String
    Public RawTotalAutorizado As String

    Public VoucherComercio As String
    Public VoucherCliente As String
    Public RutaVoucherComercio As String
    Public RutaVoucherCliente As String

    Public Monto As Double

    Public TransactionDurationTS As TimeSpan

    Private DebugMode As Boolean
    Private VB6Aux As Object

    Private DCI As Object, DRI As Object

    Private Azul_Request_Info As Object

    Public Property RequestInfo() As Object
        Get
            Return Azul_Request_Info
        End Get
        Set(ByVal value As Object)
            Azul_Request_Info = value
        End Set
    End Property

    Private Azul_Response_Info As Object

    Public Property ResponseInfo() As Object
        Get
            Return Azul_Response_Info
        End Get
        Set(ByVal value As Object)
            Azul_Response_Info = value
        End Set
    End Property

    Public Property VB6AuxObj() As Object
        Get
            Return VB6Aux
        End Get
        Set(ByVal value As Object)
            VB6Aux = value
        End Set
    End Property

    Public Property Prop_DebugMode() As Boolean
        Get
            Return DebugMode
        End Get
        Set(ByVal value As Boolean)
            DebugMode = value
        End Set
    End Property

    Private Sub SetRequestDataCallback(ByVal AsyncHandler As IAsyncResult)

        Dim ReqData As RequestState = CType(AsyncHandler.AsyncState, RequestState)

        Try


            Dim Request As HttpWebRequest = ReqData.RequestObj

            ' End the operation

            Dim PostStream As Stream = Request.EndGetRequestStream(AsyncHandler)
            ReqData.ReqStream = PostStream

            'Console.WriteLine("Please enter the input data to be posted:")

            Dim PostData As String = ReqData.SendData ' = Console.ReadLine()

            '  Convert the string into byte array.
            Dim ByteArray As Byte() = Encoding.UTF8.GetBytes(PostData)

            ' Write to the stream.
            PostStream.Write(ByteArray, 0, PostData.Length)
            PostStream.Close()

            ReqData.RequestWriteDone = True

            If Not (ReqData.Failed Or ReqData.TimedOut) Then

                ' Start the asynchronous operation to get the response

                Dim RespAsyncHandler As IAsyncResult =
                CType(Request.BeginGetResponse(
                AddressOf GetResponseCallback, ReqData), IAsyncResult)

                If Request.Timeout > 0 Then

                    ThreadPool.RegisterWaitForSingleObject(RespAsyncHandler.AsyncWaitHandle,
                    New WaitOrTimerCallback(AddressOf TimeoutCallback),
                    ReqData, Request.Timeout, True)

                End If

                Return

            End If

        Catch Web As WebException

            'Console.WriteLine(ControlChars.Lf + "SetRequestDataCallback Exception raised!")
            'Console.WriteLine(ControlChars.Lf + "Message:{0}", Web.Message)
            'Console.WriteLine(ControlChars.Lf + "Status:{0}", Web.Status)

            ReqData.Failed = True
            ReqData.LastWebException = Web

        Catch Any As Exception

            ReqData.Failed = True
            ReqData.LastWebException = Any

        End Try

        ReqData.CurrentlyProcessing = False

        AllStepsDone.Set()

    End Sub

    Private Sub GetResponseCallback(AsyncHandler As IAsyncResult)

        Dim ReqData As RequestState = CType(AsyncHandler.AsyncState, RequestState)

        Try

            ' State of request is asynchronous.

            Dim Request As HttpWebRequest = ReqData.RequestObj

            ReqData.ResponseObj = CType(Request.EndGetResponse(AsyncHandler), HttpWebResponse)

            ' Read the response into a Stream object.
            Dim RespStream As Stream = ReqData.ResponseObj.GetResponseStream()
            ReqData.RespStream = RespStream

            ReqData.GetResponseDone = True

            ' Begin the Reading of the Response Data.
            Dim ReadAsyncHandler As IAsyncResult = RespStream.BeginRead(ReqData.BufferRead, 0, BufferSize,
            New AsyncCallback(AddressOf ReadDataCallBack), ReqData)

            Return

        Catch Web As WebException

            'Console.WriteLine(ControlChars.Lf + "RespCallback Exception raised!")
            'Console.WriteLine(ControlChars.Lf + "Message:{0}", Web.Message)
            'Console.WriteLine(ControlChars.Lf + "Status:{0}", Web.Status)

            ReqData.Failed = True
            ReqData.LastWebException = Web

        Catch Any As Exception

            ReqData.Failed = True
            ReqData.LastWebException = Any

        End Try

        ReqData.CurrentlyProcessing = False
        AllStepsDone.Set()

    End Sub

    Private Sub ReadDataCallBack(AsyncHandler As IAsyncResult)

        Dim ReqData As RequestState = CType(AsyncHandler.AsyncState, RequestState)

        Try

            Dim RespStream As Stream = ReqData.RespStream
            Dim Read As Integer = RespStream.EndRead(AsyncHandler)

            If Read > 0 Then ' If Pending Data to Read is still being received

                ReqData.ResponseData.Append(Encoding.UTF8.GetString(ReqData.BufferRead, 0, Read))

                Dim ResultAsyncHandler As IAsyncResult = RespStream.BeginRead(ReqData.BufferRead, 0, BufferSize,
                New AsyncCallback(AddressOf ReadDataCallBack), ReqData)

                Return

            Else

                'Console.WriteLine(ControlChars.Lf + "The contents of the Html page are : ")

                If ReqData.ResponseData.Length > 1 Then

                    ' Up from here the Response is Received and can be handled.

                    'Dim StringContent As String
                    'StringContent = ReqData.ResponseData.ToString()
                    'Console.WriteLine(StringContent)

                End If

                'Console.WriteLine("Press any key to continue..........")
                'Console.ReadLine()

                RespStream.Close()

                ReqData.CurrentlyProcessing = False
                ReqData.Failed = False
                ReqData.TimedOut = False
                ReqData.ReadResponseDone = True

            End If

        Catch Web As WebException

            'Console.WriteLine(ControlChars.Lf + "ReadDataCallBack Exception raised!")
            'Console.WriteLine(ControlChars.Lf + "Message:{0}", Web.Message)
            'Console.WriteLine(ControlChars.Lf + "Status:{0}", Web.Status)

            ReqData.Failed = True
            ReqData.LastWebException = Web

        Catch Any As Exception

            ReqData.Failed = True
            ReqData.LastWebException = Any

        End Try

        ReqData.CurrentlyProcessing = False

        AllStepsDone.Set()

    End Sub

    Private Sub LimpiarVariables()

        Try

            Fecha = String.Empty
            Codigo = String.Empty
            Mensaje = String.Empty
            Referencia = String.Empty
            Autorizacion = String.Empty
            RawSignatureText = String.Empty
            VoucherComercio = String.Empty
            VoucherCliente = String.Empty
            RutaVoucherComercio = String.Empty
            RutaVoucherCliente = String.Empty
            TarjetaNumero = String.Empty
            TarjetaBIN = String.Empty
            RawDescuentoAplicado = "0.00"
            RawTotalAutorizado = "0.00"

        Catch ex As Exception
            'MsgBox(Err.Description, , "Limpiar Variables")
            VB6Aux.ShowBigDebugMessage("Error limpiando variables: " & Err.Description & " " & "(" & Err.Number.ToString & ")")
        End Try

    End Sub

    Public Function Venta(Monto As Double, Cuotas As String, MontoImp As Double) As Boolean

        Dim UseClientMCM As Boolean, ProcessAsync As Boolean, ReqTimeout As Integer,
        GrabarLogEnBD As Boolean, pDatosLogDB As Object

        Dim ClientMCM As AZUL.MCM.Client, ReqMCM As AZUL.MCM.Requests.SaleRequest,
        RespMCM As AZUL.MCM.Responses.BaseMCMResponse

        Dim TmpPromoData As String = String.Empty

        Dim ClientMCM_KeepAlive As Boolean

        ClientMCM_KeepAlive = True ' Por defecto está activo.

        Dim MensajeLog As String
        Dim ReqData As RequestState

        DCI = Azul_Request_Info

        DRI = Nothing

        If TypeOf DCI Is Dictionary(Of String, Object) Then

            If (DCI.ContainsKey("RespObj")) Then
                DRI = DCI.Item("RespObj")
            End If

            If (DCI.ContainsKey("UseClientMCM")) Then
                UseClientMCM = DCI.Item("UseClientMCM")
            End If

            If (DCI.ContainsKey("ClientMCM_KeepAlive")) Then
                ClientMCM_KeepAlive = DCI.Item("ClientMCM_KeepAlive")
            End If

            If (DCI.ContainsKey("ProcessAsync")) Then
                ProcessAsync = DCI.Item("ProcessAsync")
            End If

            If (DCI.ContainsKey("GrabarLogEnBD")) Then
                GrabarLogEnBD = DCI.Item("GrabarLogEnBD")
            End If

            If (DCI.ContainsKey("ReqTimeout")) Then
                ReqTimeout = DCI.Item("ReqTimeout")
            Else
                ReqTimeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
                ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.
            End If

            If (DCI.ContainsKey("PromoData")) Then
                TmpPromoData = DCI.Item("PromoData")
            End If

        Else

            If (DCI.Exists("RespObj")) Then
                DRI = DCI("RespObj")
            End If

            If (DCI.Exists("UseClientMCM")) Then
                UseClientMCM = DCI("UseClientMCM")
            End If

            If (DCI.Exists("ClientMCM_KeepAlive")) Then
                ClientMCM_KeepAlive = DCI("ClientMCM_KeepAlive")
            End If

            If (DCI.Exists("ProcessAsync")) Then
                ProcessAsync = DCI("ProcessAsync")
            End If

            If (DCI.Exists("GrabarLogEnBD")) Then
                GrabarLogEnBD = DCI("GrabarLogEnBD")
            End If

            If (DCI.Exists("ReqTimeout")) Then
                ReqTimeout = DCI("ReqTimeout")
            Else
                ReqTimeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
                ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.
            End If

            If (DCI.Exists("PromoData")) Then
                TmpPromoData = DCI("PromoData")
            End If

        End If

        Dim Request As HttpWebRequest
        Dim Response As HttpWebResponse
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime
        Dim TmpStartTS As DateTime, TmpEndTS As DateTime
        Dim DurationSum As TimeSpan, StepTS As TimeSpan

        Dim Step1TS As New TimeSpan(0, 0, 0, 0, 0)
        Dim Step2TS As New TimeSpan(0, 0, 0, 0, 0)
        Dim Step3TS As New TimeSpan(0, 0, 0, 0, 0)
        Dim Step4TS As New TimeSpan(0, 0, 0, 0, 0)
        Dim Step5TS As New TimeSpan(0, 0, 0, 0, 0)
        Dim Step6TS As New TimeSpan(0, 0, 0, 0, 0)

        ContentLogRequest = String.Empty
        DurationSum = New TimeSpan(0, 0, 0, 0, 0)

        Try

            'If DebugMode Then MsgBox("C1")

            If (Input_InvoiceNumber = String.Empty Or Not IsNumeric(Input_InvoiceNumber)) Then
                Input_InvoiceNumber = New Random().Next(1, 999).ToString()
            End If

            Reintento = False

TryAgain:

            LimpiarVariables()

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?Sale"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?Sale"
            End If

            If Not UseClientMCM Then

                If Not DCI("AutoDetectProxy") Then
                    System.Net.WebRequest.DefaultWebProxy = Nothing
                End If

                If DCI("Expect100Continue") Then
                    ServicePointManager.Expect100Continue = True
                Else
                    ServicePointManager.Expect100Continue = False
                End If

                If DCI("UseNagleAlgorithm") Then
                    ServicePointManager.UseNagleAlgorithm = True
                Else
                    ServicePointManager.UseNagleAlgorithm = False
                End If

                If DCI("DefaultConnectionLimit") > 0 Then
                    ServicePointManager.DefaultConnectionLimit = DCI("DefaultConnectionLimit")
                End If

                Request = WebRequest.Create(Link)

                If Not DCI("AutoDetectProxy") Then
                    Request.Proxy = Nothing
                End If

                Request.Method = "POST"
                Request.ContentType = "application/json"

                Request.Timeout = ReqTimeout
                'Request.AllowWriteStreamBuffering = False
                'Request.ReadWriteTimeout = 160000

                'If DebugMode Then MsgBox("C2" & vbNewLine & Usuario & vbNewLine & Clave)

                Request.Headers.Add("Auth1", Usuario)
                Request.Headers.Add("Auth2", Clave)

            End If

            Data = ("{ " &
            """Amount"":""" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & """, " +
            """Installment"":""" + Cuotas + """, ""Itbis"":""" + FormatNumber(MontoImp, 2, TriState.True, TriState.False, TriState.False) & """, " +
            """MerchantId"":""" + MerchantID + """, ""OrderNumber"":""" + Input_InvoiceNumber + """, " +
            """PromoData"":""" + TmpPromoData + """, " +
            """TerminalId"":""" + TerminalID + """, ""UseMultiMessaging"":""1""" +
            " }")

            If UseClientMCM Then

                ReqTimeout = (ReqTimeout / 1000)

                ClientMCM = New AZUL.MCM.Client(MerchantID, TerminalID, Host, ReqTimeout)

                ClientMCM.KeepAlive = ClientMCM_KeepAlive

                ClientMCM.SetAuth(Usuario, Clave)

                If (Not String.IsNullOrEmpty(HostAlternativo)) Then

                    ClientMCM.SetFailoverHostName(HostAlternativo)

                End If

                Dim RutaLogMCM As String = Microsoft.VisualBasic.Left(System.Windows.Forms.Application.StartupPath, 2) +
                "\AzulRDLogBackup\MCM"

                Dim RutaVoucherMCM As String = Microsoft.VisualBasic.Left(System.Windows.Forms.Application.StartupPath, 2) +
                "\AzulRDLogBackup\MCM\Vouchers"

                ' Estas rutas para las pruebas si no existen, se deben crear con anticipación.

                ClientMCM.SetLogPath(RutaLogMCM)
                ClientMCM.SetVoucherPath(RutaVoucherMCM)

                ReqMCM = New AZUL.MCM.Requests.SaleRequest()

                ReqMCM.Amount = Convert.ToDecimal(Monto)
                ReqMCM.Itbis = Convert.ToDecimal(MontoImp)
                ReqMCM.OrderNumber = Input_InvoiceNumber
                ReqMCM.Installment = Int32.Parse(Cuotas)
                ReqMCM.UseMultiMessaging = True
                ReqMCM.PromoData = TmpPromoData
                ReqMCM.CreditCardNumber = String.Empty
                ReqMCM.CreditCardExpiration = String.Empty
                ReqMCM.CreditCardCVC = String.Empty

            End If

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            RequestStartTS = DateTime.Now

            ContentLogRequest = "URL: " & Link & vbNewLine & "Cuerpo de Request: " & vbNewLine & vbNewLine & Data &
            vbNewLine & vbNewLine & "Tiempo de Inicio: " & RequestStartTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt")

            DirLogRequest = "$(CurDrive)\AzulRDLogBackup\Request"

            ArchivoLogRequest = Microsoft.VisualBasic.Strings.Format(RequestStartTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
            "Venta" & "_" & Input_InvoiceNumber & "_" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & ".log"

            TmpStartTS = DateTime.Now

            VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

            If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                Thread.Sleep(100)
                VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
            End If

            If GrabarLogEnBD Then

                pDatosLogDB = VB6Aux.GetMeADictionary()

                pDatosLogDB("TipoSolicitud") = "Venta"
                pDatosLogDB("ModoSolicitud") = IIf(UseClientMCM,
                IIf(ProcessAsync, "MCM_ASYNC", "MCM"), IIf(ProcessAsync, "APP_ASYNC", "APP"))
                pDatosLogDB("OrderNumber") = Input_InvoiceNumber
                pDatosLogDB("RequestData") = Data
                pDatosLogDB("Monto") = Monto
                pDatosLogDB("FechaInicioSolicitud") = RequestStartTS.ToString("yyyy-MM-ddTHH:mm:ss")

                VB6Aux.Azul_GrabarLogBD(pDatosLogDB, False)

            End If

            TmpEndTS = DateTime.Now

            StepTS = (TmpEndTS - TmpStartTS)

            Step1TS = Step1TS.Add(StepTS)

            DurationSum = DurationSum.Add(StepTS)

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el log de solicitud: " &
                CustomDurationES(StepTS))
            End If

            'End If

            TmpStartTS = DateTime.Now

            Dim RespHttp As HttpStatusCode

            If UseClientMCM Then

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("ClientMCM_KeepAlive: " + ClientMCM.KeepAlive.ToString())
                End If

                RespMCM = ClientMCM.Send(ReqMCM)

            Else

                If ProcessAsync Then

                    AllStepsDone = New ManualResetEvent(False)

                    ReqData = New RequestState()
                    ReqData.RequestObj = Request

                    ReqData.SendData = Data

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Iniciando transaccion modo asincrono")
                    End If

                    ReqData.CurrentlyProcessing = True

                    ' Start the asynchronous operation.		
                    Dim PostAsyncHandler As IAsyncResult =
                    CType(Request.BeginGetRequestStream(
                    AddressOf SetRequestDataCallback, ReqData), IAsyncResult)

                    '' Start the asynchronous request.
                    'Dim AsyncHandler As IAsyncResult = CType(Request.BeginGetResponse(
                    'New AsyncCallback(AddressOf RespCallback), ReqData), IAsyncResult)

                    '' this line implements the timeout, if there is a timeout, the callback fires and the request aborts.
                    'ThreadPool.RegisterWaitForSingleObject(result.AsyncWaitHandle, New WaitOrTimerCallback(AddressOf TimeoutCallback), myHttpWebRequest, myResponse.DefaultTimeout, True)

                    ' El timeout lo vamos a implementar al momento de obtener la respuesta, no en el Post de SendData

                    ' The response came in the allowed time. The work processing will happen in the 
                    ' callback function.

                    'AllStepsDone.WaitOne() ' Esperar a que se cumpla todo el proceso o falle.

                    Do While ReqData.CurrentlyProcessing

                        System.Windows.Forms.Application.DoEvents()

                    Loop

                    AllStepsDone.Close()

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Regreso el control del modo asincrono.")
                    End If

                    If ReqData.Failed Or ReqData.TimedOut Then

                        If ReqData.GetResponseDone Then
                            Response = ReqData.ResponseObj
                        End If

                        If Not ReqData.LastWebException Is Nothing Then
                            Throw New WebException(ReqData.LastWebException.Message(), ReqData.LastWebException)
                        ElseIf Not ReqData.LastException Is Nothing Then
                            Throw New Exception(ReqData.LastException.Message(), ReqData.LastException)
                        End If

                    End If

                Else

                    System.Windows.Forms.Application.DoEvents()

                    Dim encoding As New System.Text.UTF8Encoding()

                    Dim Bytes As Byte() = encoding.GetBytes(Data)

                    Using RequestStream As Stream = Request.GetRequestStream()
                        'RequestStream.WriteTimeout = Request.Timeout
                        'RequestStream.ReadTimeout = Request.Timeout
                        'Send the data
                        RequestStream.Write(Bytes, 0, Bytes.Length)
                    End Using

                    Response = Request.GetResponse()

                End If

            End If

            TmpEndTS = DateTime.Now

            StepTS = (TmpEndTS - TmpStartTS)

            Step2TS = Step2TS.Add(StepTS)

            DurationSum = DurationSum.Add(StepTS)

            Dim RespOK As Boolean

            If UseClientMCM Then

                RespOK = True 'ClientMCM.HasResponse

            Else

                If ProcessAsync Then

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage(
                        "CurrentlyProcessing: " & ReqData.CurrentlyProcessing.ToString() & vbNewLine &
                        "Failed: " & ReqData.Failed.ToString() & vbNewLine &
                        "Timeout: " & ReqData.TimedOut.ToString() & vbNewLine &
                        "RequestWriteDone: " & ReqData.RequestWriteDone.ToString() & vbNewLine &
                        "GetResponseDone: " & ReqData.GetResponseDone.ToString() & vbNewLine &
                        "ReadResponseDone: " & ReqData.ReadResponseDone.ToString() & vbNewLine & vbNewLine &
                        "ResponseData: " & ReqData.ResponseData.ToString()
                        )
                    End If

                    Response = ReqData.ResponseObj

                End If

                RespHttp = Response.StatusCode

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString() &
                    vbNewLine & "Tiempo que se tardó en procesar la solicitud (Request.GetResponse): " &
                    CustomDurationES(StepTS))
                End If

                RespOK = (RespHttp = HttpStatusCode.OK)

            End If

            If RespOK Then

                If UseClientMCM Then

                    TmpStartTS = DateTime.Now

                    Dim result As String =
                    "Codigo: " & RespMCM.ResponseCode & vbNewLine &
                    "Mensaje: " & RespMCM.ResponseMessage & vbNewLine &
                    "ErrorDesc: " & RespMCM.ErrorDescription & vbNewLine &
                    "OrderNumber: " & RespMCM.OrderNumber & vbNewLine &
                    "Receipt: " & vbNewLine & RespMCM.Receipt & vbNewLine 'RespMCM.ToString()

                    If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado. " + vbNewLine + vbNewLine +
                    result) ' Dejar que esto se grabe en un archivo mejor...

                    For Each Field In RespMCM.ResponseFields

                        If Strings.UCase(Field.Name) = "AUT" Then
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("AUT:")
                            Autorizacion = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("AUT: " & Autorizacion)
                        End If

                        If Strings.UCase(Field.Name) = "REF" Then
                            'If (DebugMode) Then MsgBox("REF:")
                            Referencia = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("REF: " & Referencia)
                        End If

                        If Strings.UCase(Field.Name) = "SGN" Then
                            'If (DebugMode) Then MsgBox("SGN:")
                            RawSignatureText = Field.Value
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("SGN: " & RawSignatureText)
                        End If

                        If Strings.UCase(Field.Name) = "CRN" Then
                            TarjetaNumero = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("CRN: " & TarjetaNumero)
                        End If

                        If Strings.UCase(Field.Name) = "BIN" Then
                            TarjetaBIN = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("BIN: " & TarjetaBIN)
                        End If

                        If Strings.UCase(Field.Name) = "PRD" Then
                            RawDescuentoAplicado = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("PRD: " & RawDescuentoAplicado)
                        End If

                        If Strings.UCase(Field.Name) = "TAT" Then
                            RawTotalAutorizado = Field.Value
                            If (DebugMode) Then VB6Aux.ShowBigDebugMessage("TAT: " & RawTotalAutorizado)
                        End If
                    Next

                    Codigo = RespMCM.ResponseCode
                    Mensaje = RespMCM.ResponseMessage &
                    IIf(Not String.IsNullOrEmpty(RespMCM.ErrorDescription),
                    " - Error Desc.: " & RespMCM.ErrorDescription, String.Empty)
                    VoucherComercio = RespMCM.ReceiptMerchant
                    VoucherCliente = RespMCM.ReceiptClient

                    If Not String.IsNullOrEmpty(VoucherComercio) Or Not String.IsNullOrEmpty(VoucherCliente) Then

                        'RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\VoucherComercio_" + Referencia + ".txt"
                        'RutaVoucherCliente = My.Computer.FileSystem.CurrentDirectory + "\VoucherCliente_" + Referencia + ".txt"
                        'My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)
                        'My.Computer.FileSystem.WriteAllText(RutaVoucherCliente, VoucherCliente, False)

                        RutaVoucherComercio = VB6Aux.ResolverRutaDinamica("$(AppPath)") & "\VoucherComercio_" + Referencia + ".txt"
                        RutaVoucherCliente = VB6Aux.ResolverRutaDinamica("$(AppPath)") & "\VoucherCliente_" + Referencia + ".txt"
                        VB6Aux.SaveQuickLogFile(VoucherComercio, "$(AppPath)", "VoucherComercio_" + Referencia + ".txt")
                        VB6Aux.SaveQuickLogFile(VoucherCliente, "$(AppPath)", "VoucherCliente_" + Referencia + ".txt")

                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step3TS = Step3TS.Add(StepTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar los archivos del voucher a imprimir: " &
                        CustomDurationES(StepTS))
                    End If

                    Dim RespStr As String = String.Empty

                    If Codigo = "1[49]" Then
                        Venta = True
                        RespStr =
                        "Codigo:" & RespMCM.ResponseCode & vbNewLine &
                        "Mensaje:" & RespMCM.ResponseMessage & vbNewLine &
                        "ErrorDesc: " & RespMCM.ErrorDescription & vbNewLine &
                        "OrderNumber: " & RespMCM.OrderNumber & vbNewLine &
                        "AUT: " & Autorizacion & vbNewLine &
                        "REF: " & Referencia & vbNewLine &
                        "BIN: " & TarjetaBIN & vbNewLine &
                        "PRD: " & RawDescuentoAplicado & vbNewLine &
                        "TAT: " & RawTotalAutorizado & vbNewLine &
                        "Receipt: " & vbNewLine & RespMCM.Receipt & vbNewLine
                    Else
                        Venta = False
                        RespStr = Codigo & " - " & Mensaje & IIf(Not String.IsNullOrEmpty(RespMCM.ErrorDescription),
                        " - Error Desc.: " & RespMCM.ErrorDescription, String.Empty)
                    End If

                    'If (Not String.IsNullOrEmpty(RespStr)) Then

                    TmpStartTS = DateTime.Now

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Venta" & "_" & Referencia & IIf(Venta, "_APPROVED", "_FAILED") & ".log"

                    Thread.Sleep(100)
                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)
                    End If

                    If GrabarLogEnBD Then

                        pDatosLogDB("Resp_Codigo") = Codigo
                        pDatosLogDB("Resp_Mensaje") = Mensaje
                        pDatosLogDB("Resp_ErrorDesc") = RespMCM.ErrorDescription
                        pDatosLogDB("Resp_Detalle") = RespStr
                        pDatosLogDB("Resp_TotalAutorizado") = RawTotalAutorizado
                        pDatosLogDB("Resp_FechaFin") = RequestEndTS.ToString("yyyy-MM-ddTHH:mm:ss")
                        pDatosLogDB("DuracionSeg") = Convert.ToInt32(DurationSum.TotalSeconds)
                        pDatosLogDB("Resp_Ex_HResult") = 0
                        pDatosLogDB("Resp_Ex_StackTrace") = String.Empty

                        VB6Aux.Azul_GrabarLogBD(pDatosLogDB, True)

                    End If

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(Venta, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "ClientMCM.KeepAlive: " + ClientMCM_KeepAlive.ToString() & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES(Step1TS, "Tiempo de Log de la Solicitud: ") & vbNewLine &
                    CustomDurationES(Step2TS, "Tiempo de Procesar Solicitud (MCMClient.Send): ") & vbNewLine &
                    CustomDurationES(Step3TS, "Tiempo de Guardar Vouchers: ") & vbNewLine &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    Thread.Sleep(100)
                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step6TS = Step6TS.Add(StepTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción finalizada: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                    'End If

                Else

                    Dim result As String

                    If Not ProcessAsync Then

                        TmpStartTS = DateTime.Now

                        'If DebugMode Then MsgBox("Response.GetResponseStream")
                        Dim dataStream As New StreamReader(Response.GetResponseStream)

                        'If DebugMode Then MsgBox("dataStream.ReadToEnd")
                        result = dataStream.ReadToEnd

                        dataStream.Close()
                        Response.Close()

                        System.Windows.Forms.Application.DoEvents()

                        TmpEndTS = DateTime.Now

                        StepTS = (TmpEndTS - TmpStartTS)

                        Step3TS = Step3TS.Add(StepTS)
                        DurationSum = DurationSum.Add(StepTS)

                        If DebugMode Then
                            VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en procesar / leer la respuesta " &
                            "(GetResponseStream, ReadtoEnd): " & CustomDurationES(StepTS))
                        End If

                        If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " +
                        vbNewLine + vbNewLine + result)

                    Else

                        result = ReqData.ResponseData.ToString()

                        Response.Close()

                        System.Windows.Forms.Application.DoEvents()

                    End If

                    TmpStartTS = DateTime.Now

                    Dim Resp As JsonResponse

                    Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                    'Dim Aut As String = String.Empty

                    For Index As Integer = 0 To Resp.ResponseFields.Length - 1

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "AUT" Then
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("AUT:")
                            Autorizacion = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage(Autorizacion)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "REF" Then
                            'If (DebugMode) Then MsgBox("REF:")
                            Referencia = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then MsgBox(Referencia)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "SGN" Then
                            'If (DebugMode) Then MsgBox("SGN:")
                            RawSignatureText = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then MsgBox(RawSignatureText)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "CRN" Then
                            TarjetaNumero = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("CRN:" & TarjetaNumero)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "BIN" Then
                            TarjetaBIN = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("BIN:" & TarjetaBIN)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "PRD" Then
                            RawDescuentoAplicado = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("BIN:" & TarjetaBIN)
                        End If

                        If Strings.UCase(Resp.ResponseFields(Index).Name) = "TAT" Then
                            RawTotalAutorizado = Resp.ResponseFields(Index).Value.ToString
                            'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("BIN:" & TarjetaBIN)
                        End If

                    Next

                    'If DebugMode Then MsgBox("C4")

                    Codigo = Resp.ResponseCode
                    Mensaje = Resp.ResponseMessage &
                    IIf(Not String.IsNullOrEmpty(Resp.ErrorDescription),
                    " - Error Desc.: " & Resp.ErrorDescription, String.Empty)
                    VoucherComercio = Resp.ReceiptMerchant
                    VoucherCliente = Resp.ReceiptClient

                    'If DebugMode Then MsgBox("C5")

                    If Not String.IsNullOrEmpty(VoucherComercio) Or Not String.IsNullOrEmpty(VoucherCliente) Then

                        'RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\VoucherComercio_" + Referencia + ".txt"
                        'RutaVoucherCliente = My.Computer.FileSystem.CurrentDirectory + "\VoucherCliente_" + Referencia + ".txt"
                        'My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)
                        'My.Computer.FileSystem.WriteAllText(RutaVoucherCliente, VoucherCliente, False)

                        RutaVoucherComercio = VB6Aux.ResolverRutaDinamica("$(AppPath)") & "\VoucherComercio_" + Referencia + ".txt"
                        RutaVoucherCliente = VB6Aux.ResolverRutaDinamica("$(AppPath)") & "\VoucherCliente_" + Referencia + ".txt"
                        VB6Aux.SaveQuickLogFile(VoucherComercio, "$(AppPath)", "VoucherComercio_" + Referencia + ".txt")
                        VB6Aux.SaveQuickLogFile(VoucherCliente, "$(AppPath)", "VoucherCliente_" + Referencia + ".txt")

                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step4TS = Step4TS.Add(StepTS)
                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en parsear la respuesta y obtener los datos: " &
                        CustomDurationES(StepTS))
                    End If

                    TmpStartTS = DateTime.Now

                    'If DebugMode Then MsgBox("C6")

                    If Codigo = "1[49]" Then
                        Venta = True
                    Else
                        Venta = False
                    End If

                    Dim RespStr As String = String.Empty

                    'If DebugMode Then MsgBox("C7")

                    RespStr = result

                    ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                    'If DebugMode Then MsgBox("C8")

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step5TS = Step5TS.Add(StepTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar los archivos del voucher a imprimir: " &
                        CustomDurationES(StepTS))
                    End If

                    'If (Not String.IsNullOrEmpty(RespStr)) Then

                    TmpStartTS = DateTime.Now

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Venta" & "_" & Referencia & IIf(Venta, "_APPROVED", "_FAILED") & ".log"

                    Thread.Sleep(100)
                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)
                    End If

                    If GrabarLogEnBD Then

                        pDatosLogDB("Resp_Codigo") = Codigo
                        pDatosLogDB("Resp_Mensaje") = Mensaje
                        pDatosLogDB("Resp_ErrorDesc") = Resp.ErrorDescription
                        pDatosLogDB("Resp_Detalle") = RespStr
                        pDatosLogDB("Resp_TotalAutorizado") = RawTotalAutorizado
                        pDatosLogDB("Resp_FechaFin") = RequestEndTS.ToString("yyyy-MM-ddTHH:mm:ss")
                        pDatosLogDB("DuracionSeg") = Convert.ToInt32(DurationSum.TotalSeconds)
                        pDatosLogDB("Resp_Ex_HResult") = 0
                        pDatosLogDB("Resp_Ex_StackTrace") = String.Empty

                        VB6Aux.Azul_GrabarLogBD(pDatosLogDB, True)

                    End If

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(Venta, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES(Step1TS, "Tiempo de Log de la Solicitud: ") & vbNewLine &
                    CustomDurationES(Step2TS, "Tiempo de Procesar Solicitud (Request.GetResponse): ") & vbNewLine &
                    CustomDurationES(Step3TS, "Tiempo de Procesar Respuesta (GetResponseStream, ReadToEnd): ") & vbNewLine &
                    CustomDurationES(Step4TS, "Tiempo de Deserializar JSON y Asignar Variables: ") & vbNewLine &
                    CustomDurationES(Step5TS, "Tiempo de Guardar Vouchers: ") & vbNewLine &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    Thread.Sleep(100)
                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step6TS = Step6TS.Add(StepTS)
                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción finalizada: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                    'End If

                End If

            Else

                If UseClientMCM Then
                    Err.Raise(999, , "La solicitud no tuvo respuesta, hubo una falla de comunicación con la pasarela de pagos.")
                Else
                    'Response.Close() ' Cerrar dentro de la excepcion.
                    Err.Raise(Convert.ToInt32(RespHttp), , "La solicitud no tuvo respuesta, hubo una falla de comunicación " +
                    "con la pasarela de pagos. HttpStatusCode: " & RespHttp.ToString())
                End If

            End If

            If UseClientMCM Then
                ClientMCM = Nothing
            End If

            If ProcessAsync Then
                ReqData = Nothing
            End If

            'If DebugMode Then MsgBox("C9")

            TransactionDurationTS = DurationSum

            If Not DRI Is Nothing Then
                ' Devolver la unidad minima, milisegundos
                DRI("DLLProcessTimeMillis") = TransactionDurationTS.TotalMilliseconds
                DRI("DLLProcessTimeSeconds") = TransactionDurationTS.TotalSeconds
            End If

            GC.Collect()

            Return Venta

        Catch ex As Exception

            If UseClientMCM Then

                Codigo = Err.Number.ToString()
                Mensaje = Err.Description '& IIf(Codigo <> "999", " / " & ex.Message(), String.Empty)
                MensajeLog = Mensaje

                If Codigo <> "999" Then
                    MensajeLog = MensajeLog & vbNewLine &
                    "HResult: " & ex.HResult.ToString() & vbNewLine &
                    "StackTrace: " & ex.StackTrace & vbNewLine
                End If

                If (Step2TS.TotalMilliseconds = 0) Then
                    TmpEndTS = DateTime.Now
                    StepTS = (TmpEndTS - TmpStartTS)
                    Step2TS = Step2TS.Add(StepTS)
                    DurationSum = DurationSum.Add(StepTS)
                End If

                TmpStartTS = DateTime.Now
                RequestEndTS = DateTime.Now

                ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & MensajeLog & vbNewLine & vbNewLine &
                "ClientMCM.KeepAlive: " + ClientMCM_KeepAlive.ToString() & vbNewLine & vbNewLine &
                "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                "-----------------------------------" & vbNewLine & vbNewLine &
                IIf(Step1TS.TotalMilliseconds > 0, CustomDurationES(Step1TS, "Tiempo de Log de la Solicitud: ") & vbNewLine, String.Empty) &
                IIf(Step2TS.TotalMilliseconds > 0, CustomDurationES(Step2TS, "Tiempo de Procesar Solicitud (ClientMCM.Send): ") & vbNewLine, String.Empty) &
                CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine

                VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                    Thread.Sleep(100)
                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
                End If

                If GrabarLogEnBD Then

                    pDatosLogDB("Resp_Codigo") = Codigo
                    pDatosLogDB("Resp_Mensaje") = ex.Message()
                    pDatosLogDB("Resp_ErrorDesc") = String.Empty
                    pDatosLogDB("Resp_Detalle") = String.Empty
                    pDatosLogDB("Resp_TotalAutorizado") = RawTotalAutorizado
                    pDatosLogDB("Resp_FechaFin") = RequestEndTS.ToString("yyyy-MM-ddTHH:mm:ss")
                    pDatosLogDB("DuracionSeg") = Convert.ToInt32(DurationSum.TotalSeconds)
                    pDatosLogDB("Resp_Ex_HResult") = ex.HResult
                    pDatosLogDB("Resp_Ex_StackTrace") = ex.StackTrace & " - " & "ClientMCM.KeepAlive: " + ClientMCM_KeepAlive.ToString()

                    VB6Aux.Azul_GrabarLogBD(pDatosLogDB, True)

                End If

                TmpEndTS = DateTime.Now

                StepTS = (TmpEndTS - TmpStartTS)

                Step6TS = Step6TS.Add(StepTS)
                DurationSum = DurationSum.Add(StepTS)

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción fallida: " &
                    CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                    "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                End If

                'End If

                TransactionDurationTS = DurationSum

                If Not DRI Is Nothing Then
                    ' Devolver la unidad minima, milisegundos
                    DRI("DLLProcessTimeMillis") = TransactionDurationTS.TotalMilliseconds
                    DRI("DLLProcessTimeSeconds") = TransactionDurationTS.TotalSeconds
                End If

                ClientMCM = Nothing

                GC.Collect()

                Return False

            Else

                If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                    Codigo = Err.Number.ToString()
                    Mensaje = Err.Description '& IIf(Codigo <> "999", " / " & ex.Message(), String.Empty)
                    MensajeLog = Mensaje

                    If Codigo <> "999" Then
                        MensajeLog = MensajeLog & vbNewLine &
                        "HResult: " & ex.HResult.ToString() & vbNewLine &
                        "StackTrace: " & ex.StackTrace & vbNewLine
                    End If

                    If (Step2TS.TotalMilliseconds = 0) Then
                        TmpEndTS = DateTime.Now
                        StepTS = (TmpEndTS - TmpStartTS)
                        Step2TS = Step2TS.Add(StepTS)
                        DurationSum = DurationSum.Add(StepTS)
                    End If

                    TmpStartTS = DateTime.Now
                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & MensajeLog & vbNewLine & vbNewLine &
                    "ClientMCM.KeepAlive: " + ClientMCM_KeepAlive.ToString() & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    IIf(Step1TS.TotalMilliseconds > 0, CustomDurationES(Step1TS, "Tiempo de Log de la Solicitud: ") & vbNewLine, String.Empty) &
                    IIf(Step2TS.TotalMilliseconds > 0, CustomDurationES(Step2TS, "Tiempo de Procesar Solicitud (Request.GetResponse): ") & vbNewLine, String.Empty) &
                    IIf(Step3TS.TotalMilliseconds > 0, CustomDurationES(Step3TS, "Tiempo de Procesar Respuesta (GetResponseStream, ReadToEnd): ") & vbNewLine, String.Empty) &
                    IIf(Step4TS.TotalMilliseconds > 0, CustomDurationES(Step4TS, "Tiempo de Deserializar JSON y Asignar Variables: ") & vbNewLine, String.Empty) &
                    IIf(Step5TS.TotalMilliseconds > 0, CustomDurationES(Step5TS, "Tiempo de Guardar Vouchers: ") & vbNewLine, String.Empty) &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
                    End If

                    If GrabarLogEnBD Then

                        pDatosLogDB("Resp_Codigo") = Codigo
                        pDatosLogDB("Resp_Mensaje") = ex.Message()
                        pDatosLogDB("Resp_ErrorDesc") = String.Empty
                        pDatosLogDB("Resp_Detalle") = String.Empty
                        pDatosLogDB("Resp_TotalAutorizado") = RawTotalAutorizado
                        pDatosLogDB("Resp_FechaFin") = RequestEndTS.ToString("yyyy-MM-ddTHH:mm:ss")
                        pDatosLogDB("DuracionSeg") = Convert.ToInt32(DurationSum.TotalSeconds)
                        pDatosLogDB("Resp_Ex_HResult") = ex.HResult
                        pDatosLogDB("Resp_Ex_StackTrace") = ex.StackTrace & " - " & "ClientMCM.KeepAlive: " + ClientMCM_KeepAlive.ToString()

                        VB6Aux.Azul_GrabarLogBD(pDatosLogDB, True)

                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step6TS = Step6TS.Add(StepTS)
                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción fallida: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                    'End If

                    If Not ProcessAsync Then
                        If Not Response Is Nothing Then
                            Response.Close()
                        End If
                    End If

                    System.Threading.Thread.Sleep(1000)
                    Reintento = True
                    GoTo TryAgain

                Else

                    Codigo = Err.Number
                    Mensaje = Err.Description ' & IIf(Codigo <> "999", " / " & ex.Message(), String.Empty)
                    MensajeLog = Mensaje

                    If Codigo <> "999" Then
                        MensajeLog = MensajeLog & vbNewLine &
                        "HResult: " & ex.HResult.ToString() & vbNewLine &
                        "StackTrace: " & ex.StackTrace & vbNewLine
                    End If

                    If (Step2TS.TotalMilliseconds = 0) Then
                        TmpEndTS = DateTime.Now
                        StepTS = (TmpEndTS - TmpStartTS)
                        Step2TS = Step2TS.Add(StepTS)
                        DurationSum = DurationSum.Add(StepTS)
                    End If

                    TmpStartTS = DateTime.Now
                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & MensajeLog & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    IIf(Step1TS.TotalMilliseconds > 0, CustomDurationES(Step1TS, "Tiempo de Log de la Solicitud: ") & vbNewLine, String.Empty) &
                    IIf(Step2TS.TotalMilliseconds > 0, CustomDurationES(Step2TS, "Tiempo de Procesar Solicitud (Request.GetResponse): ") & vbNewLine, String.Empty) &
                    IIf(Step3TS.TotalMilliseconds > 0, CustomDurationES(Step3TS, "Tiempo de Procesar Respuesta (GetResponseStream, ReadToEnd): ") & vbNewLine, String.Empty) &
                    IIf(Step4TS.TotalMilliseconds > 0, CustomDurationES(Step4TS, "Tiempo de Deserializar JSON y Asignar Variables: ") & vbNewLine, String.Empty) &
                    IIf(Step5TS.TotalMilliseconds > 0, CustomDurationES(Step5TS, "Tiempo de Guardar Vouchers: ") & vbNewLine, String.Empty) &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    If Not My.Computer.FileSystem.FileExists(VB6Aux.ResolverRutaDinamica(DirLogRequest) & "\" & ArchivoLogRequest) Then
                        Thread.Sleep(100)
                        VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)
                    End If

                    If GrabarLogEnBD Then

                        pDatosLogDB("Resp_Codigo") = Codigo
                        pDatosLogDB("Resp_Mensaje") = ex.Message()
                        pDatosLogDB("Resp_ErrorDesc") = String.Empty
                        pDatosLogDB("Resp_Detalle") = String.Empty
                        pDatosLogDB("Resp_TotalAutorizado") = RawTotalAutorizado
                        pDatosLogDB("Resp_FechaFin") = RequestEndTS.ToString("yyyy-MM-ddTHH:mm:ss")
                        pDatosLogDB("DuracionSeg") = Convert.ToInt32(DurationSum.TotalSeconds)
                        pDatosLogDB("Resp_Ex_HResult") = ex.HResult
                        pDatosLogDB("Resp_Ex_StackTrace") = ex.StackTrace

                        VB6Aux.Azul_GrabarLogBD(pDatosLogDB, True)

                    End If

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    Step6TS = Step6TS.Add(StepTS)
                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción fallida: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                    'End If

                    TransactionDurationTS = DurationSum

                    If Not DRI Is Nothing Then
                        ' Devolver la unidad minima, milisegundos
                        DRI("DLLProcessTimeMillis") = TransactionDurationTS.TotalMilliseconds
                        DRI("DLLProcessTimeSeconds") = TransactionDurationTS.TotalSeconds
                    End If

                    If Not ProcessAsync Then
                        If Not Response Is Nothing Then
                            Response.Close()
                        End If
                    End If

                    If ProcessAsync Then
                        ReqData = Nothing
                    End If

                    GC.Collect()

                    Return False

                End If

            End If

        End Try

    End Function

    Public Function Anulacion(Monto As Double, MontoImp As Double, NumeroAutorizacion As String) As Boolean

        DCI = Azul_Request_Info

        DRI = Nothing

        If TypeOf DCI Is Dictionary(Of String, Object) Then
            If (DCI.ContainsKey("RespObj")) Then
                DRI = DCI.Item("RespObj")
            End If
        Else
            If (DCI.Exists("RespObj")) Then
                DRI = DCI("RespObj")
            End If
        End If

        Dim Request As HttpWebRequest
        Dim Response As HttpWebResponse
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime
        Dim TmpStartTS As DateTime, TmpEndTS As DateTime
        Dim DurationSum As TimeSpan, StepTS As TimeSpan

        ContentLogRequest = String.Empty
        DurationSum = New TimeSpan(0, 0, 0, 0, 0)

        Try

            Reintento = False

TryAgain:

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?SaleCancellation"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?SaleCancellation"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Timeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ ""Amount"":""" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """AuthorizationNumber"":""" & NumeroAutorizacion & """, ""Itbis"":""" & FormatNumber(MontoImp, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """MerchantId"":""" & MerchantID & """, ""OrderNumber"":""1"", ""TerminalId"":""" & TerminalID & """}")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            'If (Not String.IsNullOrEmpty(Data)) Then

            RequestStartTS = DateTime.Now

            ContentLogRequest = "URL: " & Link & vbNewLine & "Cuerpo de Request: " & vbNewLine & vbNewLine & Data &
            vbNewLine & vbNewLine & "Tiempo de Inicio: " & RequestStartTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt")

            DirLogRequest = "$(CurDrive)\AzulRDLogBackup\Request"

            ArchivoLogRequest = Microsoft.VisualBasic.Strings.Format(RequestStartTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
            "Anulacion" & "_" & NumeroAutorizacion & "_" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & ".log"

            TmpStartTS = DateTime.Now

            VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

            TmpEndTS = DateTime.Now

            StepTS = (TmpEndTS - TmpStartTS)

            DurationSum = DurationSum.Add(StepTS)

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el log de solicitud: " &
                CustomDurationES(StepTS))
            End If

            'End If

            TmpStartTS = DateTime.Now

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Response = Request.GetResponse

            Dim RespHttp As HttpStatusCode

            RespHttp = Response.StatusCode

            TmpEndTS = DateTime.Now

            StepTS = (TmpEndTS - TmpStartTS)

            DurationSum = DurationSum.Add(StepTS)

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString() &
                vbNewLine & "Tiempo que se tardó en procesar la solicitud (Request.GetResponse): " &
                CustomDurationES(StepTS))
            End If

            If RespHttp = HttpStatusCode.OK Then

                TmpStartTS = DateTime.Now

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()

                System.Windows.Forms.Application.DoEvents()

                TmpEndTS = DateTime.Now

                StepTS = (TmpEndTS - TmpStartTS)

                DurationSum = DurationSum.Add(StepTS)

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en procesar la respuesta (GetResponseStream, ReadtoEnd): " &
                    CustomDurationES(StepTS))
                End If

                If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " + vbNewLine + vbNewLine +
                result)

                TmpStartTS = DateTime.Now

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                For Index As Integer = 0 To Resp.ResponseFields.Length - 1

                    If Resp.ResponseFields(Index).Name = "AUT" Then
                        Autorizacion = Resp.ResponseFields(Index).Value.ToString
                    End If

                    If Resp.ResponseFields(Index).Name = "REF" Then
                        Referencia = Resp.ResponseFields(Index).Value.ToString
                    End If

                    If Resp.ResponseFields(Index).Name = "SGN" Then
                        RawSignatureText = Resp.ResponseFields(Index).Value.ToString
                    End If

                Next

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                VoucherComercio = Resp.ReceiptMerchant
                VoucherCliente = Resp.ReceiptClient

                RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\VoucherComercio_" + Referencia + ".txt"
                RutaVoucherCliente = My.Computer.FileSystem.CurrentDirectory + "\VoucherCliente_" + Referencia + ".txt"

                TmpEndTS = DateTime.Now

                StepTS = (TmpEndTS - TmpStartTS)

                DurationSum = DurationSum.Add(StepTS)

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en parsear la respuesta y obtener los datos: " &
                    CustomDurationES(StepTS))
                End If

                TmpStartTS = DateTime.Now

                My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)
                My.Computer.FileSystem.WriteAllText(RutaVoucherCliente, VoucherCliente, False)

                If Codigo = "1[49]" Then
                    Anulacion = True
                Else
                    Anulacion = False
                End If

                Dim RespStr As String = String.Empty

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                TmpEndTS = DateTime.Now

                StepTS = (TmpEndTS - TmpStartTS)

                DurationSum = DurationSum.Add(StepTS)

                If DebugMode Then
                    VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar los archivos del voucher a imprimir: " &
                    CustomDurationES(StepTS))
                End If

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    TmpStartTS = DateTime.Now

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Anulacion" & "_" & Referencia & IIf(Anulacion, "_APPROVED", "_FAILED") & ".log"

                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(Anulacion, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción finalizada: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                End If

            Else
                'Response.Close() ' En el manejo de la excepcion
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            TransactionDurationTS = DurationSum

            If Not DRI Is Nothing Then
                ' Devolver la unidad minima, milisegundos
                DRI("DLLProcessTimeMillis") = TransactionDurationTS.TotalMilliseconds
                DRI("DLLProcessTimeSeconds") = TransactionDurationTS.TotalSeconds
            End If

            Return Anulacion

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    TmpStartTS = DateTime.Now

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción fallida: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    TmpStartTS = DateTime.Now

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES(DurationSum, "Suma de tiempo de los pasos ejecutados: ") & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                    TmpEndTS = DateTime.Now

                    StepTS = (TmpEndTS - TmpStartTS)

                    DurationSum = DurationSum.Add(StepTS)

                    If DebugMode Then
                        VB6Aux.ShowBigDebugMessage("Tiempo que se tardó en guardar el archivo de log de transacción fallida: " &
                        CustomDurationES(StepTS) & vbNewLine & vbNewLine &
                        "Tiempo total de solicitud: " & CustomDurationES(DurationSum))
                    End If

                End If

                TransactionDurationTS = DurationSum

                If Not DRI Is Nothing Then
                    ' Devolver la unidad minima, milisegundos
                    DRI("DLLProcessTimeMillis") = TransactionDurationTS.TotalMilliseconds
                    DRI("DLLProcessTimeSeconds") = TransactionDurationTS.TotalSeconds
                End If

                If Not Response Is Nothing Then
                    Response.Close()
                End If

                Return False

            End If

        End Try

    End Function

    Public Function Devolucion(Monto As Double, MontoImp As Double) As Boolean

        DCI = Azul_Request_Info

        Dim Request As WebRequest
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime

        ContentLogRequest = String.Empty

        Try

            Reintento = False

TryAgain:

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?Refund"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?Refund"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Timeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ ""Amount"":""" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """Itbis"":""" & FormatNumber(MontoImp, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """MerchantId"":""" & MerchantID & """, ""OrderNumber"":""1"", ""TerminalId"":""" & TerminalID & """}")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            'If (Not String.IsNullOrEmpty(Data)) Then

            RequestStartTS = DateTime.Now

            ContentLogRequest = "URL: " & Link & vbNewLine & "Cuerpo de Request: " & vbNewLine & vbNewLine & Data &
            vbNewLine & vbNewLine & "Tiempo de Inicio: " & RequestStartTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt")

            DirLogRequest = "$(CurDrive)\AzulRDLogBackup\Request"

            ArchivoLogRequest = Microsoft.VisualBasic.Strings.Format(RequestStartTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
            "Devolucion" & "_" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & ".log"

            VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

            'End If

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim Response As WebResponse = Request.GetResponse
            Dim RespHttp As HttpStatusCode

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString())
            End If

            If RespHttp = HttpStatusCode.OK Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()
                System.Windows.Forms.Application.DoEvents()

                If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " + vbNewLine + vbNewLine +
                result)

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                For Index As Integer = 0 To Resp.ResponseFields.Length - 1

                    If Resp.ResponseFields(Index).Name = "AUT" Then
                        Autorizacion = Resp.ResponseFields(Index).Value.ToString
                    End If

                    If Resp.ResponseFields(Index).Name = "REF" Then
                        Referencia = Resp.ResponseFields(Index).Value.ToString
                    End If

                    If Resp.ResponseFields(Index).Name = "SGN" Then
                        RawSignatureText = Resp.ResponseFields(Index).Value.ToString
                    End If

                Next

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                VoucherComercio = Resp.ReceiptMerchant
                VoucherCliente = Resp.ReceiptClient

                RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\VoucherComercio_" + Referencia + ".txt"
                RutaVoucherCliente = My.Computer.FileSystem.CurrentDirectory + "\VoucherCliente_" + Referencia + ".txt"

                My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)
                My.Computer.FileSystem.WriteAllText(RutaVoucherCliente, VoucherCliente, False)

                If Codigo = "1[49]" Then
                    Devolucion = True
                Else
                    Devolucion = False
                End If

                Dim RespStr As String = String.Empty

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Devolucion" & "_" & Referencia & IIf(Devolucion, "_APPROVED", "_FAILED") & ".log"

                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(Devolucion, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

            Else
                Response.Close()
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            Return Devolucion

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                Return False

            End If

        End Try

    End Function

    Public Function ReportesDeCierre() As Boolean

        DCI = Azul_Request_Info

        Dim Request As WebRequest
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime

        ContentLogRequest = String.Empty

        Try

            Reintento = False

TryAgain:

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?PinpadSettle"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?PinpadSettle"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            'Request.Timeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' El cierre que no tenga timeout ya que no es una transacción normal. Hay que esperar lo que haya que esperar.

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ ""MerchantId"":""" & MerchantID & """, ""TerminalId"":""" & TerminalID & """}")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            'If (Not String.IsNullOrEmpty(Data)) Then

            RequestStartTS = DateTime.Now

            ContentLogRequest = "URL: " & Link & vbNewLine & "Cuerpo de Request: " & vbNewLine & vbNewLine & Data &
            vbNewLine & vbNewLine & "Tiempo de Inicio: " & RequestStartTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt")

            DirLogRequest = "$(CurDrive)\AzulRDLogBackup\Request"

            ArchivoLogRequest = Microsoft.VisualBasic.Strings.Format(RequestStartTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
            "Cierre" & "" & ".log"

            VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

            'End If

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim Response As WebResponse = Request.GetResponse
            Dim RespHttp As HttpStatusCode

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString())
            End If

            If RespHttp = HttpStatusCode.OK Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()
                System.Windows.Forms.Application.DoEvents()

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                Fecha = Resp.DateTime

                VoucherComercio = Resp.Receipt

                For Index As Integer = 0 To Resp.ResponseFields.Length - 1
                    If Resp.ResponseFields(Index).Name = "BTC" Then
                        Referencia = Resp.ResponseFields(Index).Value.ToString
                        Exit For
                    End If
                Next

                Dim mVoucherOriginal As String, mVoucherLineas() As String

                mVoucherOriginal = VoucherComercio

                If Not DCI Is Nothing Then

                    If DCI("LineaVoucherIncluirCaja") >= 0 Then

                        Dim TmpDec1 As Double

                        TmpDec1 = Math.Abs(Math.Round(Fix(DCI("LineaVoucherIncluirCaja")) - DCI("LineaVoucherIncluirCaja"), 1, MidpointRounding.AwayFromZero))

                        If Fix(DCI("LineaVoucherIncluirCaja")) = 0 Then
                            If TmpDec1 = 0.1 Then
                                VoucherComercio = VB6Aux.Cls_CentrarCadena(VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter, "Caja " + DCI("Transaccion_CodCaja"))
                            ElseIf TmpDec1 = 0.2 Then
                                VoucherComercio = VB6Aux.Cls_RellenarCadenasSeparadas("Caja:", DCI("Transaccion_CodCaja"), VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter)
                            Else
                                VoucherComercio = "Caja: " + DCI("Transaccion_CodCaja")
                            End If
                            VoucherComercio += vbNewLine + mVoucherOriginal
                        ElseIf Fix(DCI("LineaVoucherIncluirCaja")) = 999999 Then
TmpUltLinea:
                            VoucherComercio = mVoucherOriginal + vbNewLine
                            If TmpDec1 = 0.1 Then
                                VoucherComercio += VB6Aux.Cls_CentrarCadena(VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter, "Caja " + DCI("Transaccion_CodCaja"))
                            ElseIf TmpDec1 = 0.2 Then
                                VoucherComercio += VB6Aux.Cls_RellenarCadenasSeparadas("Caja:", DCI("Transaccion_CodCaja"), VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter)
                            Else
                                VoucherComercio += "Caja: " + DCI("Transaccion_CodCaja")
                            End If
                        Else

                            mVoucherLineas = Split(IIf(String.IsNullOrEmpty(mVoucherOriginal), " ", mVoucherOriginal), vbNewLine, , CompareMethod.Text)

                            VoucherComercio = mVoucherLineas(0) + vbNewLine

                            For i = 1 To UBound(mVoucherLineas)
                                If Fix(DCI("LineaVoucherIncluirCaja")) = (i + 1) Then
                                    If TmpDec1 = 0.1 Then
                                        VoucherComercio += VB6Aux.Cls_CentrarCadena(VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter, "Caja " + DCI("Transaccion_CodCaja"))
                                    ElseIf TmpDec1 = 0.2 Then
                                        VoucherComercio += VB6Aux.Cls_RellenarCadenasSeparadas("Caja:", DCI("Transaccion_CodCaja"), VB6Aux.Prop_AnchoLineaImpresoraHeaderFooter)
                                    Else
                                        VoucherComercio += "Caja: " + DCI("Transaccion_CodCaja")
                                    End If
                                    VoucherComercio += vbNewLine + mVoucherLineas(i)
                                Else
                                    VoucherComercio += vbNewLine + mVoucherLineas(i)
                                End If
                            Next

                            If ((UBound(mVoucherLineas) + 1)) < Fix(DCI("LineaVoucherIncluirCaja")) Then GoTo TmpUltLinea

                        End If

                    End If

                End If

                RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\Cierre" + Fecha + ".txt"

                My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)

                If Codigo = "1[49]" Then
                    ReportesDeCierre = True
                Else
                    ReportesDeCierre = False
                End If

                Dim RespStr As String = String.Empty

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Cierre" & "_" & Referencia & IIf(ReportesDeCierre, "_APPROVED", "_FAILED") & ".log"

                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(ReportesDeCierre, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

            Else
                Response.Close()
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            Return ReportesDeCierre

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                VB6Aux.ShowBigDebugMessage("Error al intentar realizar cierre: " & vbNewLine & vbNewLine & Codigo & " " & Mensaje)

                Return False

            End If

        End Try

    End Function

    Public Function PrecierreTotales() As Boolean

        DCI = Azul_Request_Info

        Dim Request As WebRequest
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime

        ContentLogRequest = String.Empty

        Try

            Reintento = False

TryAgain:

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?PinpadTransactionTotals"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?PinpadTransactionTotals"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Timeout = 180000 ' 180 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ ""MerchantId"":""" & MerchantID & """, ""TerminalId"":""" & TerminalID & """}")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim Response As WebResponse = Request.GetResponse
            Dim RespHttp As HttpStatusCode

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString())
            End If

            If RespHttp = HttpStatusCode.OK Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()
                System.Windows.Forms.Application.DoEvents()

                If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " + vbNewLine + vbNewLine +
                result)

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                Fecha = Resp.DateTime

                VoucherComercio = Resp.Receipt

                RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\PreCierre" + Fecha + ".txt"

                My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, True)

                If Codigo = "1[49]" Then
                    PrecierreTotales = True
                Else
                    PrecierreTotales = False
                End If

                Dim RespStr As String = String.Empty

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    VB6Aux.SaveQuickLogFile(RespStr, "$(CurDrive)\AzulRDLogBackup",
                    Microsoft.VisualBasic.Strings.Format(DateTime.Now, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "Precierre" & "_" & "0" & IIf(PrecierreTotales, "_APPROVED", "_FAILED") & ".log")

                End If

            Else
                Response.Close()
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            Return PrecierreTotales

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                'If Not String.IsNullOrEmpty(ContentLogRequest) Then

                '    RequestEndTS = DateTime.Now

                '    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                '    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                '    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                '    "-----------------------------------" & vbNewLine & vbNewLine &
                '    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                '    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                'End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description
                VB6Aux.ShowBigDebugMessage("Error al intentar realizar PreCierre: " & vbNewLine & vbNewLine & Codigo & " " & Mensaje)
                Return False

            End If

        End Try

    End Function

    Public Function BuscarUltimaTransaccion(TipoTransaccion As String) As Boolean

        DCI = Azul_Request_Info

        Dim Request As WebRequest
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime

        ContentLogRequest = String.Empty

        Try

            Reintento = False

TryAgain:

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?GetLastTrx"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?GetLastTrx"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Timeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ ""MerchantId"":""" & MerchantID & """, ""TerminalId"":""" & TerminalID & """, ""TrxType"":""" & TipoTransaccion & """}")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim Response As WebResponse = Request.GetResponse
            Dim RespHttp As HttpStatusCode

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString())
            End If

            If RespHttp = HttpStatusCode.OK Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()
                System.Windows.Forms.Application.DoEvents()

                If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " + vbNewLine + vbNewLine +
                result)

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                'If Not IsNothing(Resp.SaleResponse) Then
                '    Dim JsonResSaleResp As SaleResponse
                '    JsonResSaleResp = JsonConvert.DeserializeObject(Of SaleResponse)(Resp.SaleResponse.ToString)
                'End If

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                Fecha = Resp.DateTime

                If Codigo = "1[49]" Then
                    VoucherComercio = Resp.SaleResponse.ReceiptMerchant
                    'VoucherComercio = Resp.ReceiptMerchant
                    RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\UltimaTransaccion" + Fecha + ".txt"
                    My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)

                    BuscarUltimaTransaccion = True
                Else
                    BuscarUltimaTransaccion = False
                End If

                Dim RespStr As String = String.Empty

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    VB6Aux.SaveQuickLogFile(RespStr, "$(CurDrive)\AzulRDLogBackup",
                    Microsoft.VisualBasic.Strings.Format(DateTime.Now, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "UltTrx" & "_" & "0" & IIf(BuscarUltimaTransaccion, "_APPROVED", "_FAILED") & ".log")

                End If

            Else
                Response.Close()
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            Return BuscarUltimaTransaccion

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                'If Not String.IsNullOrEmpty(ContentLogRequest) Then

                '    RequestEndTS = DateTime.Now

                '    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                '    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                '    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                '    "-----------------------------------" & vbNewLine & vbNewLine &
                '    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                '    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                'End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description
                VB6Aux.ShowBigDebugMessage("Error al intentar buscar última transacción: " & vbNewLine & vbNewLine & Codigo & " " & Mensaje)
                Return False

            End If

        End Try

    End Function

    Public Function LecturaDeTarjeta(
    Optional ByVal Monto As Double = 0.02,
    Optional ByVal Cuotas As String = "1",
    Optional ByVal MontoImp As Double = 0) As Boolean

        DCI = Azul_Request_Info

        Dim Request As WebRequest
        Dim Data As String
        Dim DirLogRequest As String, DirLogResponse As String
        Dim ArchivoLogRequest As String, ArchivoLogResponse As String
        Dim ContentLogRequest As String, ContentLogResponse As String
        Dim RequestStartTS As DateTime, RequestEndTS As DateTime

        ContentLogRequest = String.Empty

        Try

            'If DebugMode Then MsgBox("C1")

            If (Input_InvoiceNumber = String.Empty Or Not IsNumeric(Input_InvoiceNumber)) Then Input_InvoiceNumber = "1"

            Reintento = False

TryAgain:

            LimpiarVariables()

            If Not Reintento Then
                Link = Host + "/POSWebServices/JSON/default.aspx?Sale"
            Else
                Link = HostAlternativo + "/POSWebServices/JSON/default.aspx?Sale"
            End If

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Timeout = 160000 ' 160 Segundos Tiempo maximo de toda la operación según la documentación del servicio. 
            ' Y aparte para asegurar que no se quede la transacción procesando infinitamente ni menos de lo debido.

            'If DebugMode Then MsgBox("C2" & vbNewLine & Usuario & vbNewLine & Clave)

            Request.Headers.Add("Auth1", Usuario)
            Request.Headers.Add("Auth2", Clave)

            Data = ("{ " &
            """Amount"":""" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """Installment"":""" & Cuotas & """, ""Itbis"":""" & FormatNumber(MontoImp, 2, TriState.True, TriState.False, TriState.False) & """, " &
            """MerchantId"":""" & MerchantID & """, ""OrderNumber"":""" & Input_InvoiceNumber & """, ""PromoData"":"""", " &
            """TerminalId"":""" & TerminalID & """, ""UseMultiMessaging"":""1""" &
            " }")

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Link & vbNewLine & vbNewLine & Data)
            End If

            'If (Not String.IsNullOrEmpty(Data)) Then

            RequestStartTS = DateTime.Now

            ContentLogRequest = "URL: " & Link & vbNewLine & "Cuerpo de Request: " & vbNewLine & vbNewLine & Data &
            vbNewLine & vbNewLine & "Tiempo de Inicio: " & RequestStartTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt")

            DirLogRequest = "$(CurDrive)\AzulRDLogBackup\Request"

            ArchivoLogRequest = Microsoft.VisualBasic.Strings.Format(RequestStartTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
            "LecturaTarjeta" & "_" & Input_InvoiceNumber & "_" & FormatNumber(Monto, 2, TriState.True, TriState.False, TriState.False) & ".log"

            VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

            'End If

            Dim encoding As New System.Text.UTF8Encoding()

            Dim Bytes As Byte() = encoding.GetBytes(Data)

            System.Windows.Forms.Application.DoEvents()

            Using RequestStream As Stream = Request.GetRequestStream()
                'Send the data
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim Response As WebResponse = Request.GetResponse
            Dim RespHttp As HttpStatusCode

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("DebugMode - Resp. HttpStatusCode: " & vbNewLine & RespHttp.ToString())
            End If

            If RespHttp = HttpStatusCode.OK Then

                'If DebugMode Then MsgBox("Response.GetResponseStream")
                Dim dataStream As New StreamReader(Response.GetResponseStream)

                'If DebugMode Then MsgBox("dataStream.ReadToEnd")
                Dim result As String = dataStream.ReadToEnd

                dataStream.Close()
                Response.Close()

                System.Windows.Forms.Application.DoEvents()

                If DebugMode Then VB6Aux.ShowBigDebugMessage("Resultado Plano. " + vbNewLine + vbNewLine +
                result) ' Dejar que esto se grabe en un archivo mejor...

                Dim Resp As JsonResponse
                Resp = JsonConvert.DeserializeObject(Of JsonResponse)(result)

                'Dim Aut As String = String.Empty

                For Index As Integer = 0 To Resp.ResponseFields.Length - 1

                    If Resp.ResponseFields(Index).Name = "CRN" Then
                        TarjetaNumero = Resp.ResponseFields(Index).Value.ToString
                        If (DebugMode) Then VB6Aux.ShowBigDebugMessage("CRN:" & TarjetaNumero)
                    End If

                    If Resp.ResponseFields(Index).Name = "REF" Then
                        Referencia = Resp.ResponseFields(Index).Value.ToString
                        'If (DebugMode) Then VB6Aux.ShowBigDebugMessage("REF:" & Referencia)
                    End If

                    If Resp.ResponseFields(Index).Name = "BIN" Then
                        TarjetaBIN = Resp.ResponseFields(Index).Value.ToString
                        If (DebugMode) Then VB6Aux.ShowBigDebugMessage("BIN:" & TarjetaBIN)
                    End If

                Next

                'If Not IsNothing(Resp.ResponseFields) Then
                '    Dim JsonRespField As Responsefield
                '    'JsonRespField = 'JsonConvert.DeserializeObject(Of Responsefield)(Resp.ResponseFields)
                'End If

                'If DebugMode Then MsgBox("C4")

                Codigo = Resp.ResponseCode
                Mensaje = Resp.ResponseMessage
                VoucherComercio = Resp.ReceiptMerchant
                VoucherCliente = Resp.ReceiptClient

                'If DebugMode Then MsgBox("C5")

                'RutaVoucherComercio = My.Computer.FileSystem.CurrentDirectory + "\VoucherComercio_" + Referencia + ".txt"
                'RutaVoucherCliente = My.Computer.FileSystem.CurrentDirectory + "\VoucherCliente_" + Referencia + ".txt"

                'My.Computer.FileSystem.WriteAllText(RutaVoucherComercio, VoucherComercio, False)
                'My.Computer.FileSystem.WriteAllText(RutaVoucherCliente, VoucherCliente, False)

                'If DebugMode Then MsgBox("C6")

                LecturaDeTarjeta = Not (String.IsNullOrEmpty(TarjetaBIN) And String.IsNullOrEmpty(TarjetaNumero))

                Dim RespStr As String = String.Empty

                'If DebugMode Then MsgBox("C7")

                'Try

                'Dim Dumper As System.IO.StringWriter
                'Dumper = New System.IO.StringWriter()

                'ObjectDumper.Dumper.Dump(Resp, "Transaction Result", Dumper)

                'RespStr = Dumper.ToString() 

                RespStr = result

                'Catch ex As Exception

                'If DebugMode Then

                'VB6Aux.ShowBigDebugMessage("Error Parsing Resp " + vbNewLine + ex.Message + vbNewLine +
                'ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                'End If

                'End Try

                ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

                'If DebugMode Then MsgBox("C8")

                If (Not String.IsNullOrEmpty(RespStr)) Then

                    RequestEndTS = DateTime.Now

                    ContentLogResponse = RespStr

                    DirLogResponse = "$(CurDrive)\AzulRDLogBackup"

                    ArchivoLogResponse = Microsoft.VisualBasic.Strings.Format(RequestEndTS, "yyyy_MM_dd_HH_mm_ss") & "_" &
                    "LecturaTarjeta" & "_" & Referencia & IIf(LecturaDeTarjeta, "_SUCCESS", "_FAILED") & ".log"

                    VB6Aux.SaveQuickLogFile(ContentLogResponse, DirLogResponse, ArchivoLogResponse)

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: " & IIf(LecturaDeTarjeta, "Exitosa", "Fallida") &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Mensaje: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ") & vbNewLine & vbNewLine &
                    "Ruta Log de Respuesta: " & VB6Aux.ResolverRutaDinamica(DirLogResponse) & "\" & ArchivoLogResponse

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

            Else
                Response.Close()
                Err.Raise(Convert.ToInt32(RespHttp), , "HttpStatusCode - " & RespHttp.ToString())
            End If

            'If DebugMode Then MsgBox("C9")

            Return LecturaDeTarjeta

        Catch ex As Exception

            If Not Reintento And Len(Trim(HostAlternativo)) > 0 Then

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                System.Threading.Thread.Sleep(1000)
                Reintento = True
                GoTo TryAgain

            Else

                Codigo = Err.Number
                Mensaje = Err.Description

                If Not String.IsNullOrEmpty(ContentLogRequest) Then

                    RequestEndTS = DateTime.Now

                    ContentLogRequest = ContentLogRequest & vbNewLine & vbNewLine & "Tipo de Respuesta: Error o falla de comunicacion" &
                    vbNewLine & vbNewLine & "Codigo de Respuesta: " & Codigo & vbNewLine & "Descripcion: " & Mensaje & vbNewLine & vbNewLine &
                    "Tiempo de Finalizacion: " & RequestEndTS.ToString("yyyy-MM-dd hh:mm:ss.fff tt") & vbNewLine & vbNewLine &
                    "-----------------------------------" & vbNewLine & vbNewLine &
                    CustomDurationES((RequestEndTS - RequestStartTS), "Tiempo total de solicitud: ")

                    VB6Aux.SaveQuickLogFile(ContentLogRequest, DirLogRequest, ArchivoLogRequest)

                End If

                Return False

            End If

        End Try

    End Function

    'Public Function ResolverRutaDinamica(ByVal pRuta As String) As String

    '    Try

    '        pRuta = Replace(pRuta, "$(AppPath)", System.IO.Path.GetDirectoryName(
    '            System.Reflection.Assembly.GetExecutingAssembly().Location), , , CompareMethod.Text)
    '        pRuta = Replace(pRuta, "$(CallerAppPath)", System.IO.Path.GetDirectoryName(
    '            System.Reflection.Assembly.GetExecutingAssembly().Location), , , CompareMethod.Text)
    '        pRuta = Replace(pRuta, "$(CurDrive)", Left(Application.ExecutablePath, 2), , , CompareMethod.Text)

    '        ResolverRutaDinamica = pRuta

    '    Catch ex As Exception

    '        Return pRuta

    '    End Try

    'End Function

    Public Sub New()
        Try
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3)
            ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
        Catch
            'MsgBox(Err.Description, , )
        End Try
    End Sub

    Public Function AcceptAllCertifications(ByVal sender As Object,
    ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate,
    ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain,
    ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function

    ''' <param name="Duration">Un TimeSpan. Ejemplo Dados 2 Datetimes A y B, cuando B es después de A, Duración puede ser pasada como (B - A)</param>
    ''' <param name="Prefix">Cualquier prefijo de texto que le quieras añadir a la representación de la Duración a retornar</param>
    ''' <param name="Mode">Valores Posibles: "SIMPLE" > Representación Humana, "PRECISE" > Retorna la duración en números con precisión hasta milisegundos, "FULL": Retorna ambos</param>
    Public Function CustomDurationES(ByVal Duration As TimeSpan,
    Optional ByVal Prefix As String = "Tiempo transcurrido: ",
    Optional ByVal Mode As String = "FULL") As String

        On Error GoTo ErrorHandler

        If Duration.TotalMilliseconds < 0 Then
            Return Prefix + "Duración Negativa (Inválida)"
        End If

        Dim SuperFormat As String = String.Empty

        Select Case Mode

            Case "SIMPLE"
            Case "PRECISE"
                Mode = Mode.ToUpper()
            Case Else
                Mode = "FULL"
        End Select

        If Mode.Equals("SIMPLE") Or Mode.Equals("FULL") Then

            If Duration.Days > 0 Then

                SuperFormat = SuperFormat + "\ d\ "

                If (Duration.Days = 1) Then
                    SuperFormat = SuperFormat + "\D\í\a"
                Else
                    SuperFormat = SuperFormat + "\D\í\a\s"
                End If

            End If

            If Duration.Hours > 0 Then

                SuperFormat = SuperFormat + "\ h\ "

                If (Duration.Hours = 1) Then
                    SuperFormat = SuperFormat + "\H\o\r\a"
                Else
                    SuperFormat = SuperFormat + "\H\o\r\a\s"
                End If

            End If

            If Duration.Minutes > 0 Then

                SuperFormat = SuperFormat + "\ m\ "

                If (Duration.Minutes = 1) Then
                    SuperFormat = SuperFormat + "\M\i\n\u\t\o"
                Else
                    SuperFormat = SuperFormat + "\M\i\n\u\t\o\s"
                End If

            End If

            If Duration.Seconds > 0 Then

                SuperFormat = SuperFormat + "\ s\ "

                If (Duration.Seconds = 1) Then
                    SuperFormat = SuperFormat + "\S\e\g\u\n\d\o"
                Else
                    SuperFormat = SuperFormat + "\S\e\g\u\n\d\o\s"
                End If

                If Duration.TotalMilliseconds < 1000 Then

                    SuperFormat = SuperFormat + "\ "

                    'If (Duration.Milliseconds > 99) Then 
                    SuperFormat = SuperFormat + "fff"
                    'Else If (Duration.Milliseconds > 9) 
                    'SuperFormat = SuperFormat + "ff" 
                    'Else 
                    'SuperFormat = SuperFormat + "f" 
                    'End If

                    SuperFormat = SuperFormat + "\ \m\s"

                End If

            End If

        End If

        If Mode.Equals("PRECISE") Or Mode.Equals("FULL") Then

            If Duration.TotalSeconds > 59 Then

                SuperFormat = SuperFormat + "\ \("

                If Duration.Days > 0 Then
                    SuperFormat = SuperFormat + "dd\:"
                End If

                If Duration.TotalMinutes > 59 Then
                    SuperFormat = SuperFormat + "hh\:"
                End If

                If Duration.TotalSeconds > 59 Then
                    SuperFormat = SuperFormat + "mm\:"
                End If

                SuperFormat = SuperFormat + "ss\.fff"

                SuperFormat = SuperFormat + "\)"

            Else

                If Duration.TotalMilliseconds > 1000 Then

                    SuperFormat = SuperFormat + "\ \("

                    SuperFormat = SuperFormat + "ss\.fff"

                    SuperFormat = SuperFormat + "\)"

                End If

            End If

        End If

        Return Prefix + Duration.ToString(Microsoft.VisualBasic.Strings.Replace(SuperFormat,
        "\ ", String.Empty, 1, 1, Microsoft.VisualBasic.CompareMethod.Text))

        Exit Function

ErrorHandler:

    End Function

    ''' <param name="Duration">A TimeSpan. Example: Given Two Datetimes A And B, where B Is later, Duration can be passed As (B - A)</param>
    ''' <param name="Prefix">Any textual Prefix you would Like to add to the Duration representation to be returned</param>
    ''' <param name="Mode">Posible Values: "SIMPLE" > Returns Human representation, "PRECISE" > Returns Raw Date with precision up to Milliseconds, "FULL": Returns Both</param>
    Public Function CustomDurationEN(ByVal Duration As TimeSpan,
    Optional ByVal Prefix As String = "Time elapsed: ",
    Optional ByVal Mode As String = "FULL") As String

        On Error GoTo ErrorHandler

        If Duration.TotalMilliseconds < 0 Then
            Return "Negative Duration (invalid Params)"
        End If

        Dim SuperFormat As String = String.Empty

        Select Case Mode

            Case "SIMPLE"
            Case "PRECISE"
                Mode = Mode.ToUpper()
            Case Else
                Mode = "FULL"
        End Select

        If Mode.Equals("SIMPLE") Or Mode.Equals("FULL") Then

            If Duration.Days > 0 Then

                SuperFormat = SuperFormat + "\ d\ "

                If (Duration.Days = 1) Then
                    SuperFormat = SuperFormat + "\D\a\y"
                Else
                    SuperFormat = SuperFormat + "\D\a\y\s"
                End If

            End If

            If Duration.Hours > 0 Then

                SuperFormat = SuperFormat + "\ h\ "

                If (Duration.Hours = 1) Then
                    SuperFormat = SuperFormat + "\H\o\u\r"
                Else
                    SuperFormat = SuperFormat + "\H\o\u\r\s"
                End If

            End If

            If Duration.Minutes > 0 Then

                SuperFormat = SuperFormat + "\ m\ "

                If (Duration.Minutes = 1) Then
                    SuperFormat = SuperFormat + "\M\i\n\u\t\e"
                Else
                    SuperFormat = SuperFormat + "\M\i\n\u\t\e\s"
                End If

            End If

            If Duration.Seconds > 0 Then

                SuperFormat = SuperFormat + "\ s\ "

                If (Duration.Seconds = 1) Then
                    SuperFormat = SuperFormat + "\S\e\c\o\n\d"
                Else
                    SuperFormat = SuperFormat + "\S\e\c\o\n\d\s"
                End If

                If Duration.TotalMilliseconds < 1000 Then

                    SuperFormat = SuperFormat + "\ "

                    'If (Duration.Milliseconds > 99) Then 
                    SuperFormat = SuperFormat + "fff"
                    'Else If (Duration.Milliseconds > 9) 
                    'SuperFormat = SuperFormat + "ff" 
                    'Else 
                    'SuperFormat = SuperFormat + "f" 
                    'End If

                    SuperFormat = SuperFormat + "\ \m\s"

                End If

            End If

        End If

        If Mode.Equals("PRECISE") Or Mode.Equals("FULL") Then

            If Duration.TotalSeconds > 59 Then

                SuperFormat = SuperFormat + "\ \("

                If Duration.Days > 0 Then
                    SuperFormat = SuperFormat + "dd\:"
                End If

                If Duration.TotalMinutes > 59 Then
                    SuperFormat = SuperFormat + "hh\:"
                End If

                If Duration.TotalSeconds > 59 Then
                    SuperFormat = SuperFormat + "mm\:"
                End If

                SuperFormat = SuperFormat + "ss\.fff"

                SuperFormat = SuperFormat + "\)"

            Else

                If Duration.TotalMilliseconds > 1000 Then

                    SuperFormat = SuperFormat + "\ \("

                    SuperFormat = SuperFormat + "ss\.fff"

                    SuperFormat = SuperFormat + "\)"

                End If

            End If

        End If

        Return Prefix + Duration.ToString(Microsoft.VisualBasic.Strings.Replace(SuperFormat,
        "\ ", String.Empty, 1, 1, Microsoft.VisualBasic.CompareMethod.Text))

        Exit Function

ErrorHandler:

    End Function

End Class
