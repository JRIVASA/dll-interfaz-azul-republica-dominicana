﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("InterfazAzul")>
<Assembly: AssemblyDescription("Interfaz Pago Integrado Stellar - Azul")>
<Assembly: AssemblyCompany("BIGWISE Corp")>
<Assembly: AssemblyProduct("InterfazAzul")>
<Assembly: AssemblyCopyright("Copyright © 2023 BIGWISE Corp")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c23438f0-5bec-4781-8ed4-42e839394fd0")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.5.0.0")>
<Assembly: AssemblyFileVersion("1.5.0.0")>
