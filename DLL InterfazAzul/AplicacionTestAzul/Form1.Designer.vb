﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn_Anulacion = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txt_Monto = New System.Windows.Forms.TextBox()
        Me.txt_Cuotas = New System.Windows.Forms.TextBox()
        Me.txt_MontoImp = New System.Windows.Forms.TextBox()
        Me.txt_Usuario = New System.Windows.Forms.TextBox()
        Me.txt_HostAlterno = New System.Windows.Forms.TextBox()
        Me.txt_Host = New System.Windows.Forms.TextBox()
        Me.txt_MerchantID = New System.Windows.Forms.TextBox()
        Me.txt_TerminalD = New System.Windows.Forms.TextBox()
        Me.txt_clave = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_Guardar = New System.Windows.Forms.Button()
        Me.btn_Limpiar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_Cierre = New System.Windows.Forms.Button()
        Me.cmb_TipoTrans = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_NumAut = New System.Windows.Forms.TextBox()
        Me.BtnPreCie = New System.Windows.Forms.Button()
        Me.OptTrue1 = New System.Windows.Forms.RadioButton()
        Me.OptFalse1 = New System.Windows.Forms.RadioButton()
        Me.lblDebugMode = New System.Windows.Forms.Label()
        Me.ChkUsarDLLAzul = New System.Windows.Forms.CheckBox()
        Me.ChkProcessAsync = New System.Windows.Forms.CheckBox()
        Me.lblTimeout = New System.Windows.Forms.Label()
        Me.txtTimeout = New System.Windows.Forms.TextBox()
        Me.ChkUsarDLLAzul_KeepAlive = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPromoData = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(209, 24)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 20)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Venta"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn_Anulacion
        '
        Me.btn_Anulacion.Location = New System.Drawing.Point(209, 49)
        Me.btn_Anulacion.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Anulacion.Name = "btn_Anulacion"
        Me.btn_Anulacion.Size = New System.Drawing.Size(86, 19)
        Me.btn_Anulacion.TabIndex = 1
        Me.btn_Anulacion.Text = "Anulacion"
        Me.btn_Anulacion.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(209, 162)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 41)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Buscar Ultima Transaccion"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'txt_Monto
        '
        Me.txt_Monto.Location = New System.Drawing.Point(113, 26)
        Me.txt_Monto.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Monto.Name = "txt_Monto"
        Me.txt_Monto.Size = New System.Drawing.Size(76, 20)
        Me.txt_Monto.TabIndex = 3
        '
        'txt_Cuotas
        '
        Me.txt_Cuotas.Location = New System.Drawing.Point(113, 49)
        Me.txt_Cuotas.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Cuotas.Name = "txt_Cuotas"
        Me.txt_Cuotas.Size = New System.Drawing.Size(76, 20)
        Me.txt_Cuotas.TabIndex = 4
        '
        'txt_MontoImp
        '
        Me.txt_MontoImp.Location = New System.Drawing.Point(113, 72)
        Me.txt_MontoImp.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_MontoImp.Name = "txt_MontoImp"
        Me.txt_MontoImp.Size = New System.Drawing.Size(76, 20)
        Me.txt_MontoImp.TabIndex = 5
        '
        'txt_Usuario
        '
        Me.txt_Usuario.Location = New System.Drawing.Point(110, 72)
        Me.txt_Usuario.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Usuario.Name = "txt_Usuario"
        Me.txt_Usuario.Size = New System.Drawing.Size(167, 20)
        Me.txt_Usuario.TabIndex = 8
        '
        'txt_HostAlterno
        '
        Me.txt_HostAlterno.Location = New System.Drawing.Point(110, 49)
        Me.txt_HostAlterno.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_HostAlterno.Name = "txt_HostAlterno"
        Me.txt_HostAlterno.Size = New System.Drawing.Size(167, 20)
        Me.txt_HostAlterno.TabIndex = 7
        '
        'txt_Host
        '
        Me.txt_Host.Location = New System.Drawing.Point(110, 26)
        Me.txt_Host.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Host.Name = "txt_Host"
        Me.txt_Host.Size = New System.Drawing.Size(167, 20)
        Me.txt_Host.TabIndex = 6
        '
        'txt_MerchantID
        '
        Me.txt_MerchantID.Location = New System.Drawing.Point(110, 140)
        Me.txt_MerchantID.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_MerchantID.Name = "txt_MerchantID"
        Me.txt_MerchantID.Size = New System.Drawing.Size(167, 20)
        Me.txt_MerchantID.TabIndex = 11
        '
        'txt_TerminalD
        '
        Me.txt_TerminalD.Location = New System.Drawing.Point(110, 117)
        Me.txt_TerminalD.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_TerminalD.Name = "txt_TerminalD"
        Me.txt_TerminalD.Size = New System.Drawing.Size(167, 20)
        Me.txt_TerminalD.TabIndex = 10
        '
        'txt_clave
        '
        Me.txt_clave.Location = New System.Drawing.Point(110, 94)
        Me.txt_clave.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_clave.Name = "txt_clave"
        Me.txt_clave.Size = New System.Drawing.Size(168, 20)
        Me.txt_clave.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 28)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Host:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 51)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Host Alterno:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 74)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Usuario:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 97)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Clave:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 142)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Merchant ID:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 119)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Terminal ID:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_Guardar)
        Me.GroupBox1.Controls.Add(Me.btn_Limpiar)
        Me.GroupBox1.Controls.Add(Me.txt_Host)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txt_HostAlterno)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txt_MerchantID)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txt_TerminalD)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txt_Usuario)
        Me.GroupBox1.Controls.Add(Me.txt_clave)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 289)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(308, 216)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Configuracion"
        '
        'btn_Guardar
        '
        Me.btn_Guardar.Location = New System.Drawing.Point(200, 171)
        Me.btn_Guardar.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Guardar.Name = "btn_Guardar"
        Me.btn_Guardar.Size = New System.Drawing.Size(76, 25)
        Me.btn_Guardar.TabIndex = 19
        Me.btn_Guardar.Text = "Guardar"
        Me.btn_Guardar.UseVisualStyleBackColor = True
        '
        'btn_Limpiar
        '
        Me.btn_Limpiar.Location = New System.Drawing.Point(110, 171)
        Me.btn_Limpiar.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Limpiar.Name = "btn_Limpiar"
        Me.btn_Limpiar.Size = New System.Drawing.Size(76, 25)
        Me.btn_Limpiar.TabIndex = 18
        Me.btn_Limpiar.Text = "Limpiar"
        Me.btn_Limpiar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 30)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Monto:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(25, 51)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Cuotas"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(25, 74)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Monto Impuesto:"
        '
        'btn_Cierre
        '
        Me.btn_Cierre.Location = New System.Drawing.Point(209, 117)
        Me.btn_Cierre.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Cierre.Name = "btn_Cierre"
        Me.btn_Cierre.Size = New System.Drawing.Size(86, 34)
        Me.btn_Cierre.TabIndex = 23
        Me.btn_Cierre.Text = "Cierre"
        Me.btn_Cierre.UseVisualStyleBackColor = True
        '
        'cmb_TipoTrans
        '
        Me.cmb_TipoTrans.FormattingEnabled = True
        Me.cmb_TipoTrans.Items.AddRange(New Object() {"Sale", "Refund"})
        Me.cmb_TipoTrans.Location = New System.Drawing.Point(120, 174)
        Me.cmb_TipoTrans.Margin = New System.Windows.Forms.Padding(2)
        Me.cmb_TipoTrans.Name = "cmb_TipoTrans"
        Me.cmb_TipoTrans.Size = New System.Drawing.Size(76, 21)
        Me.cmb_TipoTrans.TabIndex = 24
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(26, 176)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Tipo Transaccion:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(26, 97)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(90, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Num Autorizacion"
        '
        'txt_NumAut
        '
        Me.txt_NumAut.Location = New System.Drawing.Point(113, 94)
        Me.txt_NumAut.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_NumAut.Name = "txt_NumAut"
        Me.txt_NumAut.Size = New System.Drawing.Size(76, 20)
        Me.txt_NumAut.TabIndex = 27
        '
        'BtnPreCie
        '
        Me.BtnPreCie.Location = New System.Drawing.Point(35, 118)
        Me.BtnPreCie.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnPreCie.Name = "BtnPreCie"
        Me.BtnPreCie.Size = New System.Drawing.Size(86, 34)
        Me.BtnPreCie.TabIndex = 28
        Me.BtnPreCie.Text = "PreCierre"
        Me.BtnPreCie.UseVisualStyleBackColor = True
        '
        'OptTrue1
        '
        Me.OptTrue1.AutoSize = True
        Me.OptTrue1.Location = New System.Drawing.Point(155, 247)
        Me.OptTrue1.Name = "OptTrue1"
        Me.OptTrue1.Size = New System.Drawing.Size(41, 17)
        Me.OptTrue1.TabIndex = 29
        Me.OptTrue1.Text = "ON"
        Me.OptTrue1.UseVisualStyleBackColor = True
        '
        'OptFalse1
        '
        Me.OptFalse1.AutoSize = True
        Me.OptFalse1.Checked = True
        Me.OptFalse1.Location = New System.Drawing.Point(240, 247)
        Me.OptFalse1.Name = "OptFalse1"
        Me.OptFalse1.Size = New System.Drawing.Size(45, 17)
        Me.OptFalse1.TabIndex = 30
        Me.OptFalse1.TabStop = True
        Me.OptFalse1.Text = "OFF"
        Me.OptFalse1.UseVisualStyleBackColor = True
        '
        'lblDebugMode
        '
        Me.lblDebugMode.AutoSize = True
        Me.lblDebugMode.Location = New System.Drawing.Point(28, 249)
        Me.lblDebugMode.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDebugMode.Name = "lblDebugMode"
        Me.lblDebugMode.Size = New System.Drawing.Size(69, 13)
        Me.lblDebugMode.TabIndex = 31
        Me.lblDebugMode.Text = "DebugMode:"
        '
        'ChkUsarDLLAzul
        '
        Me.ChkUsarDLLAzul.AutoSize = True
        Me.ChkUsarDLLAzul.Location = New System.Drawing.Point(329, 30)
        Me.ChkUsarDLLAzul.Name = "ChkUsarDLLAzul"
        Me.ChkUsarDLLAzul.Size = New System.Drawing.Size(94, 17)
        Me.ChkUsarDLLAzul.TabIndex = 32
        Me.ChkUsarDLLAzul.Text = "Usar DLL Azul"
        Me.ChkUsarDLLAzul.UseVisualStyleBackColor = True
        '
        'ChkProcessAsync
        '
        Me.ChkProcessAsync.AutoSize = True
        Me.ChkProcessAsync.Location = New System.Drawing.Point(329, 93)
        Me.ChkProcessAsync.Name = "ChkProcessAsync"
        Me.ChkProcessAsync.Size = New System.Drawing.Size(146, 17)
        Me.ChkProcessAsync.TabIndex = 33
        Me.ChkProcessAsync.Text = "Procesamiento Asincrono"
        Me.ChkProcessAsync.UseVisualStyleBackColor = True
        '
        'lblTimeout
        '
        Me.lblTimeout.AutoSize = True
        Me.lblTimeout.Location = New System.Drawing.Point(326, 128)
        Me.lblTimeout.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTimeout.Name = "lblTimeout"
        Me.lblTimeout.Size = New System.Drawing.Size(48, 13)
        Me.lblTimeout.TabIndex = 35
        Me.lblTimeout.Text = "Timeout:"
        '
        'txtTimeout
        '
        Me.txtTimeout.Location = New System.Drawing.Point(414, 124)
        Me.txtTimeout.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimeout.Name = "txtTimeout"
        Me.txtTimeout.Size = New System.Drawing.Size(76, 20)
        Me.txtTimeout.TabIndex = 34
        Me.txtTimeout.Text = "160000"
        '
        'ChkUsarDLLAzul_KeepAlive
        '
        Me.ChkUsarDLLAzul_KeepAlive.AutoSize = True
        Me.ChkUsarDLLAzul_KeepAlive.Checked = True
        Me.ChkUsarDLLAzul_KeepAlive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkUsarDLLAzul_KeepAlive.Location = New System.Drawing.Point(329, 53)
        Me.ChkUsarDLLAzul_KeepAlive.Name = "ChkUsarDLLAzul_KeepAlive"
        Me.ChkUsarDLLAzul_KeepAlive.Size = New System.Drawing.Size(145, 17)
        Me.ChkUsarDLLAzul_KeepAlive.TabIndex = 36
        Me.ChkUsarDLLAzul_KeepAlive.Text = "DLL Azul - KeepAlive ON"
        Me.ChkUsarDLLAzul_KeepAlive.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(32, 211)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "PromoData"
        '
        'txtPromoData
        '
        Me.txtPromoData.Location = New System.Drawing.Point(120, 209)
        Me.txtPromoData.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPromoData.Name = "txtPromoData"
        Me.txtPromoData.Size = New System.Drawing.Size(378, 20)
        Me.txtPromoData.TabIndex = 37
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 516)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtPromoData)
        Me.Controls.Add(Me.ChkUsarDLLAzul_KeepAlive)
        Me.Controls.Add(Me.lblTimeout)
        Me.Controls.Add(Me.txtTimeout)
        Me.Controls.Add(Me.ChkProcessAsync)
        Me.Controls.Add(Me.ChkUsarDLLAzul)
        Me.Controls.Add(Me.lblDebugMode)
        Me.Controls.Add(Me.OptFalse1)
        Me.Controls.Add(Me.OptTrue1)
        Me.Controls.Add(Me.BtnPreCie)
        Me.Controls.Add(Me.txt_NumAut)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmb_TipoTrans)
        Me.Controls.Add(Me.btn_Cierre)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txt_MontoImp)
        Me.Controls.Add(Me.txt_Cuotas)
        Me.Controls.Add(Me.txt_Monto)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btn_Anulacion)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btn_Anulacion As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txt_Monto As System.Windows.Forms.TextBox
    Friend WithEvents txt_Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents txt_MontoImp As System.Windows.Forms.TextBox
    Friend WithEvents txt_Usuario As System.Windows.Forms.TextBox
    Friend WithEvents txt_HostAlterno As System.Windows.Forms.TextBox
    Friend WithEvents txt_Host As System.Windows.Forms.TextBox
    Friend WithEvents txt_MerchantID As System.Windows.Forms.TextBox
    Friend WithEvents txt_TerminalD As System.Windows.Forms.TextBox
    Friend WithEvents txt_clave As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Guardar As System.Windows.Forms.Button
    Friend WithEvents btn_Limpiar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btn_Cierre As System.Windows.Forms.Button
    Friend WithEvents cmb_TipoTrans As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_NumAut As System.Windows.Forms.TextBox
    Friend WithEvents BtnPreCie As Button
    Friend WithEvents OptTrue1 As RadioButton
    Friend WithEvents OptFalse1 As RadioButton
    Friend WithEvents lblDebugMode As Label
    Friend WithEvents ChkUsarDLLAzul As CheckBox
    Friend WithEvents ChkProcessAsync As CheckBox
    Friend WithEvents lblTimeout As Label
    Friend WithEvents txtTimeout As TextBox
    Friend WithEvents ChkUsarDLLAzul_KeepAlive As CheckBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtPromoData As TextBox
End Class
