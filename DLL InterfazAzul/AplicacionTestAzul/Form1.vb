﻿Imports InterfazAzul
Imports System.Net

Public Class Form1

    Dim ClsInt As New ClassInterfaz
    Dim RespB As Boolean

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try

            Dim AzulRequestInfo As New Dictionary(Of String, Object)
            AzulRequestInfo.Item("RespObj") = New Dictionary(Of String, Object)

            ClsInt.Input_InvoiceNumber = String.Empty
            ClsInt.VB6AuxObj = New VB6Aux
            ClsInt.Prop_DebugMode = OptTrue1.Checked

            AzulRequestInfo.Add("AutoDetectProxy", True)
            AzulRequestInfo.Add("Expect100Continue", True)
            AzulRequestInfo.Add("UseNagleAlgorithm", True)
            AzulRequestInfo.Add("DefaultConnectionLimit", 0)
            AzulRequestInfo.Add("UseClientMCM", ChkUsarDLLAzul.Checked)
            AzulRequestInfo.Add("ClientMCM_KeepAlive", ChkUsarDLLAzul_KeepAlive.Checked)
            AzulRequestInfo.Add("ProcessAsync", ChkProcessAsync.Checked)
            AzulRequestInfo.Add("GrabarLogEnBD", False)
            AzulRequestInfo.Add("ReqTimeout", CInt(txtTimeout.Text))

            If String.IsNullOrEmpty(txtPromoData.Text.Trim()) Then
                AzulRequestInfo.Add("ContainsPromoData", False)
            Else
                AzulRequestInfo.Add("ContainsPromoData", True)
                AzulRequestInfo.Add("PromoData", txtPromoData.Text)
                AzulRequestInfo.Add("PromoDataFmt", "<PromoData>" + txtPromoData.Text + "</PromoData>")
            End If

            ClsInt.RequestInfo = AzulRequestInfo

            RespB = ClsInt.Venta(CDbl(txt_Monto.Text), txt_Cuotas.Text, CDbl(txt_MontoImp.Text))

            MsgBox(ClsInt.Codigo + "-" + ClsInt.Mensaje)

        Catch ex As Exception

            Dim CodigoStr As String = Err.Number
            Dim ErrorStr As String = Err.Description

            MsgBox(ErrorStr)

        End Try

    End Sub

    Private Sub btn_Anulacion_Click(sender As Object, e As EventArgs) Handles btn_Anulacion.Click
        Try
            Dim AzulRequestInfo As New Dictionary(Of String, Object)
            AzulRequestInfo.Item("RespObj") = New Dictionary(Of String, Object)
            ClsInt.Input_InvoiceNumber = String.Empty
            ClsInt.VB6AuxObj = New VB6Aux
            ClsInt.Prop_DebugMode = OptTrue1.Checked
            ClsInt.RequestInfo = AzulRequestInfo
            RespB = ClsInt.Anulacion(CDbl(txt_Monto.Text), CDbl(txt_MontoImp.Text), txt_NumAut.Text)
            MsgBox(ClsInt.Codigo + "-" + ClsInt.Mensaje)
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            RespB = ClsInt.BuscarUltimaTransaccion(cmb_TipoTrans.Text)
            MsgBox(ClsInt.Codigo + "-" + ClsInt.Mensaje)
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Try
        '    ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3)
        'Catch
        'End Try

        txt_Host.Text = "https://pruebas.azul.com.do"
        txt_HostAlterno.Text = "https://pruebas.azul.com.do"
        txt_Usuario.Text = "merit"
        txt_clave.Text = "merit"
        txt_MerchantID.Text = "39036630010"
        txt_TerminalD.Text = "01290010"

        ClsInt.Host = txt_Host.Text
        ClsInt.HostAlternativo = txt_HostAlterno.Text
        ClsInt.Usuario = txt_Usuario.Text
        ClsInt.Clave = txt_clave.Text
        ClsInt.MerchantID = txt_MerchantID.Text
        ClsInt.TerminalID = txt_TerminalD.Text

        ClsInt.VB6AuxObj = New VB6Aux
        ClsInt.Prop_DebugMode = True

    End Sub

    Private Sub btn_Limpiar_Click(sender As Object, e As EventArgs) Handles btn_Limpiar.Click

        txt_Host.Text = ""
        txt_HostAlterno.Text = ""
        txt_Usuario.Text = ""
        txt_clave.Text = ""
        txt_MerchantID.Text = ""
        txt_TerminalD.Text = ""

    End Sub

    Private Sub btn_Guardar_Click(sender As Object, e As EventArgs) Handles btn_Guardar.Click

        ClsInt.Host = txt_Host.Text
        ClsInt.HostAlternativo = txt_HostAlterno.Text
        ClsInt.Usuario = txt_Usuario.Text
        ClsInt.Clave = txt_clave.Text
        ClsInt.MerchantID = txt_MerchantID.Text
        ClsInt.TerminalID = txt_TerminalD.Text

    End Sub

    Private Sub btn_Cierre_Click(sender As Object, e As EventArgs) Handles btn_Cierre.Click
        Try
            RespB = ClsInt.ReportesDeCierre()
        Catch ex As Exception
            MsgBox(Err.Description, , )
        End Try
    End Sub

    Private Sub BtnPreCie_Click(sender As Object, e As EventArgs) Handles BtnPreCie.Click
        Try
            RespB = ClsInt.PrecierreTotales()
        Catch ex As Exception
            MsgBox(Err.Description, , )
        End Try
    End Sub
End Class
